<?PHP
   
   require('../../../CONNECTION/SECURITY/conex.php');
//session_start();
$string_intro = getenv("QUERY_STRING");
parse_str($string_intro);
//Exportar datos de php a Excel
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=reports_digital_form.xls");



$datos_cliente = mysqli_query($conex,"SELECT `id_datClie`, `id_user_registro`, `tipo_cliente`, `nombre`, `tipo_documento`, `num_documento`, `medio_contact`, `tel_casa`, `tel_oficina`, `celular`, `fech_nacimiento`, `email`, `estado_civil`, `pagador_mismo` FROM `datos_cliente` ORDER BY `id_datClie` DESC LIMIT 1"); 
while ($dato = mysqli_fetch_array($datos_cliente)) {
    $id_datClie = $dato['id_datClie'];
    $tipo_cliente	 =  $dato['tipo_cliente'];
    $nombre =  $dato['nombre'];
    $tipo_documento = $dato['tipo_documento'];
    $num_documento =  $dato['num_documento'];
    $medio_contact = $dato['medio_contact'];
    $tel_casa =  $dato['tel_casa'];
    $tel_oficina =  $dato['tel_oficina'];
    $celular =  $dato['celular'];
    $fech_nacimiento =  $dato['fech_nacimiento'];
    $email =  $dato['email'];
    $estado_civil =  $dato['estado_civil'];
}

  $datos_suscripcion = mysqli_query($conex,"SELECT `id_datSuscripcion`, `id_cliente`, `suscripcion`, `fecha_venta`, `fecha_ini_suscripcion`, `publicacion`, `periodo_suscripcion`, `frecuencia`, `fecha_registro` FROM `datos_suscripcion` ORDER BY `id_datSuscripcion` DESC LIMIT 1"); 
  while($dato = mysqli_fetch_array($datos_suscripcion)) {


  $id_datSuscripcion =  $dato['id_datSuscripcion'];
  $id_cliente =  $dato['id_cliente'];
  $suscripcion =  $dato['suscripcion'];
  $fecha_venta =  $dato['fecha_venta'];
  $fecha_ini_suscripcion =  $dato['fecha_ini_suscripcion'];
  $publicacion =  $dato['publicacion'];
  $periodo_suscripcion =  $dato['periodo_suscripcion'];
  $frecuencia =  $dato['frecuencia'];
  $fecha_registro =  $dato['fecha_registro'];

} 

$direccion_suscripcion = mysqli_query($conex,"SELECT `id_direcSuscripcion`, `id_cliente`, `direccion_alterna`, `departamento`, `ciudad`, `barrio`, `direccion` FROM `direccion_suscripcion` ORDER BY `id_direcSuscripcion` DESC LIMIT 1"); 
while ($dato = mysqli_fetch_array($direccion_suscripcion)) {

$id_direcSuscripcion =  $dato['id_direcSuscripcion'];
$id_cliente =  $dato['id_cliente'];
$direccion_alterna =  $dato['direccion_alterna'];
$departamento =  $dato['departamento'];
$ciudad =  $dato['ciudad'];
$barrio =  $dato['barrio'];
$direccion =  $dato['direccion'];
}
$datos_cliente_titular = mysqli_query($conex,"SELECT `id_datClieTi`, `id_user_registro`, `nit`, `nombre`, `apellido`, `tipo_documento`, `num_documento`, `medio_contact`, `genero`, `tel_casa`, `tel_oficina`, `celular`, `fech_nacimiento`, `email`, `estado_civil`, `fecha_registro` FROM `datos_cliente_titular` ORDER BY `id_datClieTi` DESC LIMIT 1"); 
while ($dato = mysqli_fetch_array($datos_cliente_titular)) {

  $id_user_registro =  $dato['id_user_registro'];
  $nit =  $dato['nit'];
  $nombre1 =  $dato['nombre'];
  $apellido1 =  $dato['apellido'];
  $tipo_documento1 =  $dato['tipo_documento'];
  $num_documento1 =  $dato['num_documento'];
  $medio_contact1 =  $dato['medio_contact'];
  $genero1 =  $dato['genero'];
  $tel_casa1 =  $dato['tel_casa'];
  $tel_oficina1 =  $dato['tel_oficina'];
  $celular1 =  $dato['celular'];
  $fech_nacimiento1 =  $dato['fech_nacimiento'];
  $email1 =  $dato['email'];
  $estado_civil1 =  $dato['estado_civil'];
  $fecha_registro1 =  $dato['fecha_registro'];
}
$direccion_suscri_alterna = mysqli_query($conex,"SELECT `id_direcSuscripcion`, `id_cliente`, `departamento`, `ciudad`, `barrio`, `direccion` FROM `direccion_suscri_alterna` ORDER BY `id_direcSuscripcion` DESC LIMIT 1"); 
while ($dato = mysqli_fetch_array($direccion_suscri_alterna)) {
  $id_cliente =  $dato['id_cliente'];
  $departamento =  $dato['departamento'];
  $ciudad =  $dato['ciudad'];
  $barrio =  $dato['barrio'];
  $direccion =  $dato['direccion'];
}
$forma_pago = mysqli_query($conex,"SELECT `id_formaPago`, `id_cliente`, `medio_pago`, `num_TC`, `tipo_TC`, `vencimiento_TC`, `banco_TC`, `num_cuotasTC`, `nie_codensa`, `debito_automatico`, `ofrece_obsequio`, `cual_obsequio`, `precio_bruto`, `descuento`, `valor_neto`, `observaciones`, `fecha_registro` FROM `forma_pago` ORDER BY `id_formaPago` DESC LIMIT 1"); 
while ($dato = mysqli_fetch_array($forma_pago)) {
  $id_cliente =  $dato['id_cliente'];
  $medio_pago =  $dato['medio_pago'];
  $num_TC =  $dato['num_TC'];
  $tipo_TC =  $dato['tipo_TC'];
  $vencimiento_TC =  $dato['vencimiento_TC'];
  $banco_TC =  $dato['banco_TC'];
  $num_cuotasTC =  $dato['num_cuotasTC'];
  $nie_codensa =  $dato['nie_codensa'];
  $debito_automatico =  $dato['debito_automatico'];
  $ofrece_obsequio =  $dato['ofrece_obsequio'];
  $cual_obsequio =  $dato['cual_obsequio'];
  $precio_bruto =  $dato['precio_bruto'];
  $descuento =  $dato['descuento'];
  $valor_neto =  $dato['valor_neto'];
  $observaciones =  $dato['observaciones'];
  $fecha_registro =  $dato['fecha_registro'];
}
?>
<table border="1px" bordercolor="#000000">
      <tr style="font-weight:bold; text-transform:uppercase; height:25; padding:3px">
          <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >CLASE DOCUMENTO</th>
          <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >NOMBRE</th>
          <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >APELLIDO</th>
          <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >TRATATIENTO(SIR.SRA)</th>
          <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >DIRECCION</th>
		  <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >TELEFONO FIJO</th>
		  <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >NO.CELULAR</th>
		  <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >FECHA DE NACIMIENTO<br>(D/M/A)</th>		 
		  <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >CEDULA VENDEDOR</th>
		  <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >PRODUCTO</th>
      <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >MEDIO DE PAGO</th>
      <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >CLASE DE TC</th>
      <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >NUMERO TC</th>
      <th class="botones" style="background-color:#21c8ff; color: #FFFFFF;" >NO.CUOTAS</th>}
      </tr>
	  <?php
      while ($fila1 = mysqli_fetch_array($consulta_paciente))
      {
      ?>
          <tr align="center" style="height:25px;">
          	 
          <td><?php echo utf8_decode($fila1['tipo_documento']);?></td>
              <td><?php echo utf8_decode($fila1['nombre']);?></td>
			  <td><?php echo utf8_decode($fila1['apellido']);?></td>
			  <td><?php echo $fila1['direccion']?></td>
			  <td><?php echo $fila1['tel_casa']?></td>
			  <td><?php echo $fila1['celular']?></td>
			  <td><?php echo $fila1['fech_nacimiento']?></td>
			  <td><?php echo $fila1['num_documento1']?></td>
			  <td><?php echo utf8_decode($fila1['suscripcion']);?></td>
			  <td><?php echo $fila1['medio_pago']?></td>
			  <td><?php echo $fila1['tipo_TC']?></td>
			  <td><?php echo $fila1['num_TC']?></td>
			  <td><?php echo utf8_decode($fila1['num_cuotasTC'])?></td>
			  </tr>
              <?php
			
      }
      ?>
 </table>