<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

if ($id_user != ''  && $id_user != '0') {
  $id_userD = base64_decode($id_user);

?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EL TIEMPO</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/jqvmap/jqvmap.min.css">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../../DESIGN/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <link rel="stylesheet" href="../../../DESIGN/assets/bootstrap-4.5.0-dist/css/main.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../DESIGN/assets/bootstrap-4.5.0-dist/css/adminlte.min.css">
    <script src="../../../DESIGN/assets/plugins/jquery/jquery.min.js"></script>
    <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccion.js"></script>
    <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccionAL.js"></script>
    <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/main.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        var via = $('#VIA').val();
        var dt_via = $('#detalle_via').val();
        $('#VIA').change(function() {
          dir();
        });

        $('#detalle_via').change(function() {
          dir();
        });
        $('#numero').change(function() {
          dir();
        });
        $('#numero2').change(function() {
          dir();
        });
        $('#interior').change(function() {
          dir();
        });
        $('#detalle_int').change(function() {
          dir();
        });
        $('#interior2').change(function() {
          dir();
        });
        $('#detalle_int2').change(function() {
          dir();
        });
        $('#interior3').change(function() {
          dir();
        });
        $('#detalle_int3').change(function() {
          dir();
        });

        $('#ciudad').select2();

        /////////////////////Terminacion///////////////// 



      });
    </script>
    <script>
      $(document).ready(function() {

        var viaAL = $('#VIAAL').val();
        var dt_viaAL = $('#detalle_viaAL').val();
        $('#VIAAL').change(function() {
          dirAL();
        });

        $('#detalle_viaAL').change(function() {
          dirAL();
        });
        $('#numeroAL').change(function() {
          dirAL();
        });
        $('#numero2AL').change(function() {
          dirAL();
        });
        $('#interiorAL').change(function() {
          dirAL();
        });
        $('#detalle_intAL').change(function() {
          dirAL();
        });
        $('#interior2AL').change(function() {
          dirAL();
        });
        $('#detalle_int2AL').change(function() {
          dirAL();
        });
        $('#interior3AL').change(function() {
          dirAL();
        });
        $('#detalle_int3AL').change(function() {
          dirAL();
        });

        $('#ciudadAL').select2();
      });
    </script>


    <script>
      $(document).ready(function() {
        $('#publicacion').change(function() {

          var publicacion = $('#publicacion').val();
          if (publicacion == 'OTRO') {

            $("#cualOtroPu").css('display', 'block');
            $('#cualOtroPu').attr('required', 'required');
          } else {
            $("#cualOtroPu").css('display', 'none');
            $("#cualOtroPu").val("");
            $('#cualOtroPu').removeAttr('required', 'required');

          }
        });

        $('#periodoSuscrip').change(function() {

          var periodoSuscrip = $('#periodoSuscrip').val();
          if (periodoSuscrip == 'OTRO') {

            $("#periodoOt").css('display', 'block');
            $('#periodoOt').attr('required', 'required');
          } else {
            $("#periodoOt").css('display', 'none');
            $("#periodoOt").val("");
            $('#periodoOt').removeAttr('required', 'required');

          }
        });

        $('#tipoTargeta').change(function() {

          var tipoTargeta = $('#tipoTargeta').val();
          if (tipoTargeta == 'otrotargetas') {

            $("#cualTargeta").css('display', 'block');
            $('#cualTargeta').attr('required', 'required');
          } else {
            $("#cualTargeta").css('display', 'none');
            $("#cualTargeta").val("");
            $('#cualTargeta').removeAttr('required', 'required');

          }
        })

        $('#targetaCredito').change(function() {

          var targetaCredito = $('#targetaCredito').val();
          if (targetaCredito == 'TARJETA DE CREDITO') {

            $('#tipoTargeta').attr('required', 'required');
            $('#cualTargeta').attr('required', 'required');
            $('#noTargeta').attr('required', 'required');
            $('#banco').attr('required', 'required');
            $('#noCuotas').attr('required', 'required');
            $('#month').attr('required', 'required');
          } else {
            $("#cualTargeta").val("");
            $('#cualTargeta').removeAttr('required', 'required');

          }
        })

        $('#codensaSi').change(function() {

          var codensaSi = $('#codensaSi').val();
          if (codensaSi == 'codensaSi') {

            $('#nie1').attr('required', 'required');
            $('#nie2').attr('required', 'required');
          } else {
            $("#codensaSi").val("");
            $('#codensaSi').removeAttr('required', 'required');

          }
        })

        $('#medioPagoSi').change(function() {

          var medioPagoSi = $('#medioPagoSi').val();
          if (medioPagoSi == 'SI') {

            $('#medioPagoOcultarOtro').attr('required', 'required');
          } else {
            $("#medioPagoOcultarOtro").val("");
            $('#medioPagoOcultarOtro').removeAttr('required', 'required');

          }
        })


        $('#frecuencia').change(function() {

          var frecuencia = $('#frecuencia').val();
          if (frecuencia == 'OTRO') {

            $("#frecuenciaOt").css('display', 'block');
            $('#frecuenciaOt').attr('required', 'required');
          } else {
            $("#frecuenciaOt").css('display', 'none');
            $("#frecuenciaOt ").val("");
            $('#frecuenciaOt').removeAttr('required', 'required');
          }
        });


        $('#mismoSuscript').change(function() {

          var mismoSuscript = $('#mismoSuscript').val();
          if (mismoSuscript != 'NO') {

            $('#nit').attr('required', 'required');
            $('#medioContactoAlterno').attr('required', 'required');
            $('#tipoDocumentoSuscrip').attr('required', 'required');
            $('#noDocumentoAlterno').attr('required', 'required')
            $('#nombresCompletosSuscrip').attr('required', 'required')
            $('#targetSuscrip').attr('required', 'required')
            $('#celularSuscripAlterno').attr('required', 'required')
            $('#fechaNacimientoSuscrip').attr('required', 'required')
            $('#emailsuscripalterno').attr('required', 'required')
            $('#estadoCivilSuscrip').attr('required', 'required')
          } else {
            $("#mismoSuscripNo ").val("");
            $('#mismoSuscripNo').removeAttr('required', 'required');
          }
        });

      });
    </script>


  </head>

  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index.php" class="nav-link">Inicio</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments"></i>
              <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Brad Diesel
                      <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">Call me whenever you can...</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      John Pierce
                      <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">I got your message bro</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Nora Silvester
                      <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">The subject goes here</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">15 Notifications</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> 4 new messages
                <span class="float-right text-muted text-sm">3 mins</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> 8 friend requests
                <span class="float-right text-muted text-sm">12 hours</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> 3 new reports
                <span class="float-right text-muted text-sm">2 days</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
          </li>

        </ul>
      </nav>
      <!-- /.navbar -->
      <?php require('../../DROPDOWN/adviser_menu.php');  ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark"></h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                  <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <!-- left column -->
              <div class="col-sm-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="col-sm-12">

                  </div>
                </div>
                <hr class="linea">
                <!-- /.card-header -->
                <!-- form start -->
                <form action="../../../FUNCTIONS/CRUD/form_suscripcion.php" method="post" enctype="multipart/form-data" name="form" id="form">
                  <div class="form-row">
                    <div class="form-group col-sm-8">
                      <div class="col-sm-3">
                        <label class="form-check-label" for="suscripcion"><b>A. SUSCRIPCI&Oacute;N</b></label><br><input type="hidden" name="id_user" value="<?php echo $id_userD; ?>">
                        <select class="form-control" name="suscripcion" id="suscripcion" required>
                          <option value="">Seleccione...</option>
                          <option value="nueva">NUEVA</option>
                          <option value="renovacion">RENOVACI&Oacute;N</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-sm-3">
                      <label for="fechaventa"><b>FECHA DE VENTA</b></label>
                      <input type="date" class="form-control" id="fechaVenta" name="fechaVenta" required>
                    </div>



                    <div class="form-group col-sm-8">
                      <label for="publicacion"> <b>B. PUBLICACI&Oacute;N</b></label><br>
                      <div class="col-sm-4">
                        <select class="form-control" name="publicacion" id="publicacion" required>
                          <option value="">Seleccione...</option>
                          <option value="EL TIEMPO">EL TIEMPO</option>
                          <option value="EL TIEMPO DG PLATINO">EL TIEMPO DG PLATINO</option>
                          <option value="EL TIEMPO DG ORO">EL TIEMPO DG ORO</option>
                          <option value="PORTAFOLIO">PORTAFOLIO</option>
                          <option value="REVISTA PORTAFOLIO">REVISTA PORTAFOLIO</option>
                          <option value="REVISTA AlO">REVISTA Al&Oacute;</option>
                          <option value="REVISTA DON JUAN">REVISTA DON JUAN</option>
                          <option value="REVISTA HABITAR">REVISTA HABITAR</option>
                          <option value="REVISTA BOCAS">REVISTA BOCAS</option>
                          <option value="OTRO">OTRO</option>
                        </select><br>
                        <input class="form-control" type="text" name="publicacionOt" id="cualOtroPu" style="display: none;">
                      </div>
                    </div>

                    <div class="form-group col-sm-3">
                      <label for="fechaInicioSuscrip">FECHA DE INICIO DE LA SUSCRIPCI&Oacute;N</label>
                      <input type="date" class="form-control" id="fechaInicioSuscrip" name="fechaInicioSuscrip" required>
                    </div>

                    <div class="form-group col-sm-8">
                      <label for="periodoSuscrip">C. PERIODO DE LA SUSCRIPCI&Oacute;N</label><br>
                      <div class="col-sm-6">
                        <select class="form-control" name="periodoSuscrip" id="periodoSuscrip" required>
                          <option value="">Seleccione...</option>
                          <option value="1 MES">1 MES</option>
                          <option value="3 MESES">3 MESES</option>
                          <option value="6 MESES">6 MESES</option>
                          <option value="12 MESES">12 MESES</option>
                          <option value="OTRO">OTRO</option>
                        </select><br>
                        <input class="form-control" type="text" name="periodoOt" id="periodoOt" style="display: none;" placeholder="Cual otro periodo...">
                      </div>
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="frecuencia">D. FRECUENCIA</label><br>
                      <div class="col-sm-12">
                        <select class="form-control" name="frecuencia" id="frecuencia" required>
                          <option value="">Seleccione...</option>
                          <option value="TODOS LOS DIAS">Todos los d&iacute;as </option>
                          <option value="LUNES">LUNES</option>
                          <option value="MARTES">MARTES</option>
                          <option value="MIERCOLES">MIERCOLES</option>
                          <option value="JUEVES">JUEVES</option>
                          <option value="VIERNES">VIERNES</option>
                          <option value="SABADO">S&aacute;bado</option>
                          <option value="DOMINGO">DOMINGO</option>
                          <option value="lunesSabado">LUNES A S&Aacute;BADO</option>
                          <option value="QUINCENAL">QUINCENAL</option>
                          <option value="MENSUAL">MENSUAL</option>
                          <option value="BIMENSUAL">BIMENSUAL</option>
                          <option value="OTRO">OTRO</option>
                        </select><br>
                        <input class="form-control" type="text" name="frecuenciaOt" id="frecuenciaOt" style="display: none;" placeholder="Con que frencuencia...">
                      </div>
                    </div>

                    <div class="form-group col-sm-12">
                      <label for="informacioncliente" class="blue">E. INFORMACI&Oacute;N DEL CLIENTE / PAGADOR</label><br>
                    </div>


                    <div class="form-group col-sm-3">
                      <label for="tipoPersona">Tipo de Persona</label><br>
                      <select class="form-control" name="tipoPersona" id="tipoPersona" required>
                        <option value="">Seleccione...</option>
                        <option value="PERSONA NATURAL">Persona Natural.</option>
                        <option value="PERSONA JURIDICA">Persona Jur&iacute;dica</option>
                      </select>
                    </div>


                    <div class="form-group col-sm-2">
                      <label for="tipoDocumento">TIPO DE DOCUMENTO</label><br>
                      <select class="form-control" name="tipoDocumento" id="tipoDocumento" required>
                        <option value="">Seleccione...</option>
                        <option value="CC">C.C</option>
                        <option value="CE">C.E</option>
                        <option value="TI">T.I</option>
                        <option value="NIT">NIT</option>
                      </select>
                    </div>


                    <div class="form-group col-sm-3">
                      <div class="row">
                        <label for="">No.</label>
                        <div class="input-group ">
                          <div class="input-group-prepend numeroDocumentoCliente">
                            <span class="input-group-text" id="basic-addon1"><i class="far fa-address-card"></i></span>
                          </div>
                          <input type="number" class="form-control" name="numDocuClie" id="numeroDocumentoCliente" placeholder="No. Documento" aria-label="Username" aria-describedby="basic-addon1" required>
                        </div>
                      </div>
                    </div>




                    <div class="form-group col-sm-4">
                      <label for="medioContacto">MEDIO PREFERIDO PARA SER CONTACTADO </label><br>
                      <select class="form-control" name="medioContacto" id="medioContacto" required>
                        <option value="">Seleccione...</option>
                        <option value="emailContacto">E-MAIL</option>
                        <option value="celularContacto">CELULAR</option>
                        <option value="telCasaContacto">TEL. CASA</option>
                        <option value="telContactoOficina">TEL. OFICINA</option>
                      </select>
                    </div>


                    <div class="form-group col-sm-12">
                      <div class="row">
                        <label for="">NOMBRES COMPLETOS Y/O RAZ&Oacute;N SOCIAL</label>
                        <div class="input-group ">
                          <div class="input-group-prepend nombreCompletoCliente">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
                          </div>
                          <input type="text" class="form-control nombreCompletoCliente" name="nombreCliePag" id="nombreCompletoCliente" placeholder="Ej: Laura Daniela Gonzales/People Marketing S.A.S" aria-label="nombreCompletoCliente" aria-describedby="basic-addon1" required>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-2">
                      <div class="row">
                        <label for="">TEL&Eacute;FONO CASA</label>
                        <div class="input-group ">
                          <div class="input-group-prepend telCasa">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-house-user"></i></i></span>
                          </div>
                          <input type="number" class="form-control telCasa" name="telCasa" id="telCasa" placeholder="228 4170" aria-label="telCasa" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-2">
                      <div class="row">
                        <label for="">TEL&Eacute;FONO OFICINA</label>
                        <div class="input-group ">
                          <div class="input-group-prepend telOficina">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-volume"></i></i></span>
                          </div>
                          <input type="number" class="form-control telOficina" name="telOficina" id="telOficina" placeholder="559 4580" aria-label="telOficina" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>


                    <div class="form-group col-sm-2">
                      <label for="extension">EXT</label><br>
                      <input class="form-control" type="number" name="extension" id="extension" placeholder="Ejemplo: (1)">
                    </div>

                    <div class="form-group col-sm-2">
                      <div class="row">
                        <label for="">CELULAR</label>
                        <div class="input-group ">
                          <div class="input-group-prepend celular">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                          </div>
                          <input type="number" class="form-control celular" name="celular" id="celular" placeholder="319-047-9242" aria-label="celular" aria-describedby="basic-addon1" required>
                        </div>
                      </div>
                    </div>


                    <div class="form-group col-sm-3">
                      <label for="fechaNacimiento">FECHA DE NACIMIENTO</label>
                      <input type="date" class="form-control" id="fechaNacimiento" name="fechaNaci" required>
                    </div>

                    <div class="form-group col-sm-6">
                      <div class="row">
                        <label for="email">Correo Electr&oacute;nico</label>
                        <div class="input-group ">
                          <div class="input-group-prepend email">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
                          </div>
                          <input type="email" class="form-control email" name="email" id="email" placeholder="example@example.com" aria-label="email" aria-describedby="basic-addon1" required>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-6">
                      <label for="estadoCivil">ESTADO CIVIL</label><br>
                      <select class="form-control" name="estadoCivil" id="estadoCivil" required>
                        <option value="">Seleccione...</option>
                        <option value="SOLTERO">SOLTERO</option>
                        <option value="CASADO">CASADO</option>
                        <option value="VIUDO">VIUDO</option>
                        <option value="UNION LIBRE">UNI&Oacute;N LIBRE</option>
                      </select>
                    </div>

                    <!-- INFORMACION ALTERNA -->

                    <div class="form-group col-sm-12 blue">
                      <label>F. INFORMACI&Oacute;N DEL SUSCRITOR / TITULAR</label><br>
                    </div>

                    <div class="form-group col-sm-3" id="mismoSuscript">
                      <label for="tipoSuscriptor"><b><u>EL PAGADOR ES EL MISMO SUSCRIPTOR O ES
                            EMPRESA:</u></b></label><br>
                      - SI <input class="form-group" type="radio" name="pagaMismo" id="mismoSuscripSi" value="SI">
                      - NO <input class="form-group" type="radio" name="pagaMismo" id="mismoSuscripNo" value="NO">
                    </div>

                    <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label><b><u>DILIGENCIAR LO SIGUIENTE SI CONTESTO NO</u></b></label>
                    </div>

                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="nit">NIT</label><br>
                      <input class="form-control" type="number" name="nit" id="nit">
                    </div>

                    <div class="form-group col-sm-4 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="medioContactoAlterno">MEDIO PREFERIDO PARA SER CONTACTADO</label><br>

                      <select class="form-control" name="medioContactoTitular" id="medioContactoAlterno">
                        <option value="">Seleccione...</option>
                        <option value="E-MAIL">E-MAIL</option>
                        <option value="CELULAR">CELULAR</option>
                        <option value="TEL. CASA">TEL. CASA</option>
                        <option value="TEL. OFICINA">TEL. OFICINA</option>
                      </select>
                    </div>

                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="tipoDocumentoSuscrip">TIPO DE DOCUMENTO</label><br>
                      <select class="form-control" name="tipoDocumentoSuscrip" id="tipoDocumentoSuscrip">
                        <option value="">Seleccione...</option>
                        <option value="CC">C.C</option>
                        <option value="CE">C.E</option>
                        <option value="TI">T.I</option>
                        <option value="NIT">NIT</option>
                      </select>
                    </div>

                    <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="noDocumentoSuscriptor">No.</label>
                        <div class="input-group ">
                          <div class="input-group-prepend noDocumentoSuscriptor">
                            <span class="input-group-text" id="basic-addon1"><i class="far fa-address-card"></i></span>
                          </div>
                          <input type="number" class="form-control" name="noDocumentoSuscriptor" id="noDocumentoAlterno" placeholder="No. Documento" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-1 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="genero">G&Eacute;NERO</label><br>
                      - M <input class="form-group" type="radio" name="genero" id="masculino" value="M">
                      - F <input class="form-group" type="radio" name="genero" id="femenino" value="F">
                    </div>

                    <div class="form-group col-sm-6 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="nombresCompletosSuscrip">NOMBRES COMPLETOS Y/O RAZ&Oacute;N SOCIAL</label>
                        <div class="input-group ">
                          <div class="input-group-prepend nombresCompletosSuscrip">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
                          </div>
                          <input type="text" class="form-control nombresCompletosSuscrip" name="nombresCompletosSuscrip" id="nombresCompletosSuscrip" placeholder="Ej: Sandra Liliana/People Marketing S.A.S" aria-label="nombresCompletosSuscrip" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>



                    <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="apellidosSuscrip">APELLIDOS</label>
                        <div class="input-group ">
                          <div class="input-group-prepend apellidosSuscrip">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-address-card"></i></span>
                          </div>
                          <input type="text" class="form-control apellidosSuscrip" name="nombreCliePag" id="apellidosSuscrip" placeholder="Leon Aldana" aria-label="apellidosSuscrip" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="targetSuscrip">TEL&Eacute;FONO CASA</label>
                        <div class="input-group ">
                          <div class="input-group-prepend telCasa">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-house-user"></i></i></span>
                          </div>
                          <input type="number" class="form-control targetSuscrip" name="targetSuscrip" id="targetSuscrip" placeholder="228 4170" aria-label="targetSuscrip" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>




                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="telOficinaSuscrip">TEL&Eacute;FONO OFICINA</label>
                        <div class="input-group ">
                          <div class="input-group-prepend telOficina">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-volume"></i></i></span>
                          </div>
                          <input type="number" class="form-control telOficinaSuscrip" name="telOficinaSuscrip" id="telOficinaSuscrip" placeholder="559 4580" aria-label="telOficinaSuscrip" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="extensionSuscrip">EXT</label><br>
                      <input class="form-control" type="number" name="extensionSuscrip" id="extensionAlterna" placeholder="Ejemplo: (1)">
                    </div>

                    <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="">CELULAR</label>
                        <div class="input-group ">
                          <div class="input-group-prepend celular">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                          </div>
                          <input type="number" class="form-control celularSuscrip" name="celularSuscrip" id="celularSuscripAlterno" placeholder="319-047-9242" aria-label="celular" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="fechaNacimientoSuscrip">FECHA DE NACIMIENTO</label>
                      <input type="date" class="form-control" id="fechaNacimientoSuscrip" name="fechaNaciSuscrip">
                    </div>


                    <div class="form-group col-sm-4 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <div class="row">
                        <label for="emailsuscrip">Correo Electr&oacute;nico</label>
                        <div class="input-group ">
                          <div class="input-group-prepend email">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
                          </div>
                          <input type="email" class="form-control email" name="emailsuscrip" id="emailsuscripalterno" placeholder="example@example.com" aria-label="emailsuscrip" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                      <label for="estadoCivilSuscrip">ESTADO CIVIL</label><br>
                      <select class="form-control" name="estadoCivilSuscrip" id="estadoCivilSuscrip">
                        <option value="">Seleccione...</option>
                        <option value="SOLTERO">SOLTERO</option>
                        <option value="CASADO">CASADO</option>
                        <option value="VIUDO">VIUDO</option>
                        <option value="UNION LIBRE">UNI&Oacute;N LIBRE</option>
                      </select>
                    </div>

                    <div class="form-group col-sm-12 blue">
                      <label for="direccionSuscripcion"><b>DIRECCI&Oacute;N DE ENTREGA DE LA SUSCRIPCI&Oacute;N</b></label>
                    </div>

                    <!----Direccion suscripcion--->

                    <div class="col-md-12 col-sm-12">
                      <input type="text" class="form-control has-feedback-left" placeholder="Direcci&oacute;n" name="DIRECCION" id="DIRECCION" readonly="readonly" />

                    </div><br>

                    <br>
                    <div class="col-sm-2">V&iacute;a Principal:
                      <select id="VIA" name="VIA" class="form-control">
                        <option value="">Seleccione...</option>
                        <option>ANILLO VIAL</option>
                        <option>AUTOPISTA</option>
                        <option>AVENIDA</option>
                        <option>BOULEVAR</option>
                        <option>CALLE</option>
                        <option>CALLEJON</option>
                        <option>CARRERA</option>
                        <option>CIRCUNVALAR</option>
                        <option>CONDOMINIO</option>
                        <option>DIAGONAL</option>
                        <option>KILOMETRO</option>
                        <option>LOTE</option>
                        <option>SALIDA</option>
                        <option>SECTOR</option>
                        <option>TRANSVERSAL</option>
                        <option>VEREDA</option>
                        <option>VIA</option>
                      </select>
                    </div>

                    <div class="col-sm-3 ">N&uacute;mero y/o letra del cruce de la v&iacute;a:
                      <input name="detalle_via" id="detalle_via" type="text" maxlength="60" class="form-control" />
                    </div>




                    <div class="form-group col-md-3">N&uacute;mero de la placa:
                      <input name="numero" id="numero" type="text" maxlength="15" class="form-control" />
                    </div>

                    <div class="form-group col-md-2">Extensi&oacute;n

                      <input name="numero2" id="numero2" type="text" maxlength="15" class="form-control" />
                    </div>


                    <br>
                    <div class="row col-md-12 col-sm-12 ">
                      <div class="col-md-2 col-sm-2 ">Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <select id="interior" name="interior" class="form-control">
                          <option value="">Seleccione...</option>
                          <option>APARTAMENTO</option>
                          <option>BARRIO</option>
                          <option>BLOQUE</option>
                          <option>CASA</option>
                          <option>CIUDADELA</option>
                          <option>CONJUNTO</option>
                          <option>CONJUNTO RESIDENCIAL</option>
                          <option>EDIFICIO</option>
                          <option>UNIDAD</option>
                          <option>ENTRADA</option>
                          <option>ESTE</option>
                          <option>ETAPA</option>
                          <option>INTERIOR</option>
                          <option>MANZANA</option>
                          <option>NORTE</option>
                          <option>OESTE</option>
                          <option>OFICINA</option>
                          <option>OCCIDENTE</option>
                          <option>ORIENTE</option>
                          <option>PENTHOUSE</option>
                          <option>PISO</option>
                          <option>PORTERIA</option>
                          <option>SOTANO</option>
                          <option>SUR</option>
                          <option>TORRE</option>
                        </select>
                      </div>
                      <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <input name="detalle_int" id="detalle_int" type="text" maxlength="30" class="form-control" />
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 "></div>

                    <br>
                    <div class="row col-md-12 col-sm-12 ">
                      <div class="col-md-2 col-sm-2 ">Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <select id="interior2" name="interior2" class="form-control">
                          <option value="">Seleccione...</option>
                          <option>APARTAMENTO</option>
                          <option>BARRIO</option>
                          <option>BLOQUE</option>
                          <option>CASA</option>
                          <option>CIUDADELA</option>
                          <option>CONJUNTO</option>
                          <option>CONJUNTO RESIDENCIAL</option>
                          <option>EDIFICIO</option>
                          <option>UNIDAD</option>
                          <option>ENTRADA</option>
                          <option>ESTE</option>
                          <option>ETAPA</option>
                          <option>INTERIOR</option>
                          <option>MANZANA</option>
                          <option>NORTE</option>
                          <option>OESTE</option>
                          <option>OFICINA</option>
                          <option>OCCIDENTE</option>
                          <option>ORIENTE</option>
                          <option>PENTHOUSE</option>
                          <option>PISO</option>
                          <option>PORTERIA</option>
                          <option>SOTANO</option>
                          <option>SUR</option>
                          <option>TORRE</option>
                        </select>
                      </div>
                      <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" class="form-control" />
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 "></div>

                    <br>
                    <div class="row col-md-12 col-sm-12 ">
                      <div class="col-md-2 col-sm-2 ">Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <select id="interior3" name="interior3" class="form-control">
                          <option value="">Seleccione...</option>
                          <option>APARTAMENTO</option>
                          <option>BARRIO</option>
                          <option>BLOQUE</option>
                          <option>CASA</option>
                          <option>CIUDADELA</option>
                          <option>CONJUNTO</option>
                          <option>CONJUNTO RESIDENCIAL</option>
                          <option>EDIFICIO</option>
                          <option>UNIDAD</option>
                          <option>ENTRADA</option>
                          <option>ESTE</option>
                          <option>ETAPA</option>
                          <option>INTERIOR</option>
                          <option>MANZANA</option>
                          <option>NORTE</option>
                          <option>OESTE</option>
                          <option>OFICINA</option>
                          <option>OCCIDENTE</option>
                          <option>ORIENTE</option>
                          <option>PENTHOUSE</option>
                          <option>PISO</option>
                          <option>PORTERIA</option>
                          <option>SOTANO</option>
                          <option>SUR</option>
                          <option>TORRE</option>
                        </select>
                      </div>
                      <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                      <div class="col-md-4 col-sm-4 ">
                        <input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" class="form-control" />
                      </div>
                    </div><br>

                    <div class="form-group col-sm-4">
                      <label for="departamento">Departamento</label>
                      <input class=" form-control" type="text" name="departamentosus" id="departamentosus">
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="ciudad">Ciudad</label>
                      <input class=" form-control" type="text" name="ciudadsus" id="ciudadsus">
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="barrio">Barrio</label>
                      <input class=" form-control" type="text" name="barrio" id="barrio">
                    </div>

                    <!-----  termina direccion suscripcion---->

                    <br><br>
                    <div class="form-group col-sm-12">
                      <label for="direccionAlterna">DIRECCI&Oacute;N ALTERNA DE ENTREGA DE LA SUSCRIPCI&Oacute;N: </label>
                      SI <input type="radio" class="form-group" name="direccionAlterna" id="direccionAlternaSi" value="SI">
                      NO <input type="radio" class="form-group" name="direccionAlterna" id="direccionAlternaNo" value="NO">
                    </div>



                    <div class="form-group col-sm-12 target" id="target" style="display: none;">
                      <label for="estadocivil"><b><u>DIRECCI&Oacute;N ALTERNA DE ENTREGA DE LA SUSCRIPCI&Oacute;N</u></b></label>
                      <!-----  Direccion Alternativa ---->

                      <div class="form-row target" id="target" style="display: none;">
                        <div class="col-md-12 col-sm-12">
                          <input type="text" class="form-control has-feedback-left" placeholder="Direcci&oacute;n" name="DIRECCIONAL" id="DIRECCIONAL" readonly="readonly" />

                        </div><br>

                        <br>
                        <div class="col-sm-2">V&iacute;a Principal:
                          <select id="VIAAL" name="VIAAL" class="form-control">
                            <option value="">Seleccione...</option>
                            <option>ANILLO VIAL</option>
                            <option>AUTOPISTA</option>
                            <option>AVENIDA</option>
                            <option>BOULEVAR</option>
                            <option>CALLE</option>
                            <option>CALLEJON</option>
                            <option>CARRERA</option>
                            <option>CIRCUNVALAR</option>
                            <option>CONDOMINIO</option>
                            <option>DIAGONAL</option>
                            <option>KILOMETRO</option>
                            <option>LOTE</option>
                            <option>SALIDA</option>
                            <option>SECTOR</option>
                            <option>TRANSVERSAL</option>
                            <option>VEREDA</option>
                            <option>VIA</option>
                          </select>
                        </div>

                        <div class="col-sm-3 ">N&uacute;mero y/o letra del cruce de la v&iacute;a:
                          <input name="detalle_viaAL" id="detalle_viaAL" type="text" maxlength="60" class="form-control" />
                        </div>




                        <div class="form-group col-md-3">N&uacute;mero de la placa:
                          <input name="numeroAL" id="numeroAL" type="text" maxlength="15" class="form-control" />
                        </div>

                        <div class="form-group col-md-2">Extensi&oacute;n

                          <input name="numero2AL" id="numero2AL" type="text" maxlength="15" class="form-control" />
                        </div>


                        <br>
                        <div class="row col-md-12 col-sm-12 ">
                          <div class="col-md-2 col-sm-2 ">Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <select id="interiorAL" name="interiorAL" class="form-control">
                              <option value="">Seleccione...</option>
                              <option>APARTAMENTO</option>
                              <option>BARRIO</option>
                              <option>BLOQUE</option>
                              <option>CASA</option>
                              <option>CIUDADELA</option>
                              <option>CONJUNTO</option>
                              <option>CONJUNTO RESIDENCIAL</option>
                              <option>EDIFICIO</option>
                              <option>UNIDAD</option>
                              <option>ENTRADA</option>
                              <option>ESTE</option>
                              <option>ETAPA</option>
                              <option>INTERIOR</option>
                              <option>MANZANA</option>
                              <option>NORTE</option>
                              <option>OESTE</option>
                              <option>OFICINA</option>
                              <option>OCCIDENTE</option>
                              <option>ORIENTE</option>
                              <option>PENTHOUSE</option>
                              <option>PISO</option>
                              <option>PORTERIA</option>
                              <option>SOTANO</option>
                              <option>SUR</option>
                              <option>TORRE</option>
                            </select>
                          </div>
                          <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <input name="detalle_intAL" id="detalle_intAL" type="text" maxlength="30" class="form-control" />
                          </div>
                        </div>

                        <div class="col-md-12 col-sm-12 "></div>

                        <br>
                        <div class="row col-md-12 col-sm-12 ">
                          <div class="col-md-2 col-sm-2 ">Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <select id="interior2AL" name="interior2AL" class="form-control">
                              <option value="">Seleccione...</option>
                              <option>APARTAMENTO</option>
                              <option>BARRIO</option>
                              <option>BLOQUE</option>
                              <option>CASA</option>
                              <option>CIUDADELA</option>
                              <option>CONJUNTO</option>
                              <option>CONJUNTO RESIDENCIAL</option>
                              <option>EDIFICIO</option>
                              <option>UNIDAD</option>
                              <option>ENTRADA</option>
                              <option>ESTE</option>
                              <option>ETAPA</option>
                              <option>INTERIOR</option>
                              <option>MANZANA</option>
                              <option>NORTE</option>
                              <option>OESTE</option>
                              <option>OFICINA</option>
                              <option>OCCIDENTE</option>
                              <option>ORIENTE</option>
                              <option>PENTHOUSE</option>
                              <option>PISO</option>
                              <option>PORTERIA</option>
                              <option>SOTANO</option>
                              <option>SUR</option>
                              <option>TORRE</option>
                            </select>
                          </div>
                          <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <input name="detalle_int2AL" id="detalle_int2AL" type="text" maxlength="30" class="form-control" />
                          </div>
                        </div>

                        <div class="col-md-12 col-sm-12 "></div>

                        <br>
                        <div class="row col-md-12 col-sm-12 ">
                          <div class="col-md-2 col-sm-2 ">Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <select id="interior3AL" name="interior3AL" class="form-control">
                              <option value="">Seleccione...</option>
                              <option>APARTAMENTO</option>
                              <option>BARRIO</option>
                              <option>BLOQUE</option>
                              <option>CASA</option>
                              <option>CIUDADELA</option>
                              <option>CONJUNTO</option>
                              <option>CONJUNTO RESIDENCIAL</option>
                              <option>EDIFICIO</option>
                              <option>UNIDAD</option>
                              <option>ENTRADA</option>
                              <option>ESTE</option>
                              <option>ETAPA</option>
                              <option>INTERIOR</option>
                              <option>MANZANA</option>
                              <option>NORTE</option>
                              <option>OESTE</option>
                              <option>OFICINA</option>
                              <option>OCCIDENTE</option>
                              <option>ORIENTE</option>
                              <option>PENTHOUSE</option>
                              <option>PISO</option>
                              <option>PORTERIA</option>
                              <option>SOTANO</option>
                              <option>SUR</option>
                              <option>TORRE</option>
                            </select>
                          </div>
                          <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                          <div class="col-md-4 col-sm-4 ">
                            <input name="detalle_int3AL" id="detalle_int3AL" type="text" maxlength="30" class="form-control" />
                          </div>
                        </div><br>

                        <div class="form-group col-sm-4">
                          <label for="departamento">Departamento</label>
                          <input class=" form-control" type="text" name="departamentosusAL" id="departamentosusAL">
                        </div>

                        <div class="form-group col-sm-4">
                          <label for="ciudad">Ciudad</label>
                          <input class=" form-control" type="text" name="ciudadsusAL" id="ciudadsusAL">
                        </div>

                        <div class="form-group col-sm-4">
                          <label for="barrio">Barrio</label>
                          <input class=" form-control" type="text" name="barrioAL" id="barrioAL">
                        </div>

                      </div>

                    </div>



                    <div class="form-group col-sm-12">
                      <p>El Suscriptor debera entregar a CEET sus datos personales y de contacto con el fin de poder
                        realizar la entrega del producto correctamente (direcci&oacute;n actual municipio, ciudad, telefonos,
                        c&eacute;dula, NIT, entre otras.) El suscriptor sera el unico responsable por la exactitud y veracidad
                        de
                        los datos que entregue o registre, declarando que mantendra indemne a CEET por cualquier novedad
                        que ocurra en la prestaci&oacute;n del servicio que se origine por la falta de exactitud o veracidad de
                        los datos entregados.</p>
                    </div>


                    <div class="form-group col-sm-12 blue">
                      <label>G. FORMA DE PAGO</label><br>
                    </div>



                    <div class="form-group col-2">
                      <label for="medioPago">- TARJETA DE CR&Eacute;DITO </label>
                      <input class="form-group" type="radio" name="medioPago" id="targetaCredito" value="TARJETA DE CREDITO">
                    </div>


                    <div class="form-group col-sm-2 ocultar" id="ocultar tipoTargeta" style="display: none;">
                      <label for="tipoTargeta"><b>TIPO DE PAGO</b></label><br>
                      <select name="tipoTargeta" class="form-control" id="tipoTargeta">
                        <option value="">Seleccione...</option>
                        <option value="visa">VISA</option>
                        <option value="mastercard">MASTERCARD</option>
                        <option value="dinners">DINNERS</option>
                        <option value="credencial">CREDENCIAL</option>
                        <option value="ae">A.E</option>
                        <option value="otrotargetas">OTRO</option>
                      </select>
                      <input class=" form-control cualTargeta" type="text" name="tipoTargeta" id="cualTargeta" style="display: none;" placeholder="Que tipo de tarjeta...">
                    </div>


                    <div class="form-group col-sm-3 ocultar" id="ocultar" style="display: none;">
                      <div class="row">
                        <label for="noTargeta">No.</label>
                        <div class="input-group ">
                          <div class="input-group-prepend noTargeta">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-credit-card"></i></span>
                          </div>
                          <input type="number" class="form-control" name="noTargeta" id="noTargeta" placeholder="No. Tarjeta" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>


                    <div class="form-group col-3 ocultar" id="ocultar" style="display: none;">
                      <div class="row">
                        <label for="banco">BANCO</label>
                        <div class="input-group ">
                          <div class="input-group-prepend banco">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-university"></i></span>
                          </div>
                          <input type="text" class="form-control" name="banco" id="banco" placeholder="Nombre del Banco..." aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-2 ocultar" id="ocultar" style="display: none;">
                      <label for="noCuotas">No. CUOTAS</label>
                      <input class=" form-control" type="number" name="noCuotas" id="noCuotas">
                    </div>



                    <div class="form-group col-3 ocultar" id="expirationMes ocultar" style="display: none;">
                      <label for="fechaventa"><b>VENCIMIENTO:</b></label>
                      <input type="month" class="form-control" id="month" name="vencimientoTC">
                    </div><br>


                    <hr class="linea">


                    <br>
                    <div class="form-group col-2 ">
                      <label for="medioPago">- CODENSA</label>
                      <input class="form-group" type="radio" name="medioPago" id="codensaSi" value="codensaSi">
                    </div>

                    <div class="form-group col-2 targett" id="targett" style="display: none;">
                      <label for="nie">- NIE CODENSA</label>
                      <input id="nie1" class=" form-control nie" type="number" name="nie">
                    </div>

                    <div class="form-group col-sm-1 targett" id="targett" style="display: none;">
                      <label for="nie">
                        <p> </p>
                      </label><br>
                      <input id="nie2" class=" form-control" type="number" name="nieDos">
                    </div>


                    <div class="form-group col-sm-3">
                      <label for="medioPago">- EFECTIVO </label>
                      <input class="form-group" type="radio" name="medioPago" id="efectivo" value="efectivo">
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="medioPago">- CHEQUE</label>
                      <input class="form-group" type="radio" name="medioPago" id="cheque" value="cheque">
                    </div>

                    <div class="form-group col-sm-3 otros-pagos" id="otros-pagos">
                      <label for="medioPago">- OTRAS FORMAS DE PAGO </label>
                      <input class="form-group" type="radio" name="medioPago" id="medioPagoSi" value="SI">

                      <br>
                      <input class=" form-control ocultarOtro" type="text" name="medioPagoOt" id="medioPagoOcultarOtro" style="display: none;">
                    </div>

                    <div class="form-group col-sm-2">
                      <label for="medioPago">- CUENTA CORRIENTE</label>
                      <input class="form-group" type="radio" name="medioPago" id="cuentaCorriente" value="SI">
                    </div>

                    <div class="form-group col-sm-4 targetCorriente" id="targetCorriente" style="display: none;">
                      DEBITO AUTOM&Aacute;TICO:
                      Si <input class="form-group" type="radio" name="debitoAutomaticoCorriente" id="debitoAutomaticoCorrienteSi" value="SI">
                      No <input class="form-group" type="radio" name="debitoAutomaticoCorriente" id="debitoAutomaticoCorrienteNo" value="NO">
                    </div>

                    <div class="form-group col-sm-3 ">
                      <label for="medioPago">- CUENTA DE AHORROS</label>
                      <input class="form-group" type="radio" name="medioPago" id="cuentaAhorros" value="cuentaAhorros">
                    </div>

                    <div class="form-group col-sm-4 targetAhorros" id="targetAhorros" style="display: none;">
                      DEBITO AUTOM&Aacute;TICO:
                      Si <input class="form-group" type="radio" name="debitoAutomaticoAhorrosSi" id="debitoAutomaticoAhorros" value="SI">
                      No <input class="form-group" type="radio" name="debitoAutomaticoAhorros" id="debitoAutomaticoAhorrosNo" value="NO">
                    </div>

                    <div class="form-group col-sm-3">
                      <label for="obsequio">- SE OFRECE OBSEQUIO </label>
                      Si <input class="form-group obsequioSi" type="radio" name="obsequio" id="obsequioSi" value="SI">
                      No <input class="form-group obsequioNo" type="radio" name="obsequio" id="obsequioNo" value="NO"> <br>
                      <input class=" form-control obsequioCual" type="text" name="obsequioCual" id="obsequioCual" style="display: none;">
                    </div>

                    <div class="form-group col-3">
                      <label for="precioBruto">- PRECIO BRUTO</label>
                      <input class=" form-control" type="number" name="precioBruto" id="precioBruto">
                    </div>

                    <div class="form-group col-3">
                      <label for="descuento">- DESCUENTO</label>
                      <input class=" form-control" type="number" name="descuento" id="descuento">
                    </div>

                    <div class="form-group col-3">
                      <label for="valorNeto">- VALOR NETO</label>
                      <input class=" form-control" type="number" name="valorNeto" id="valorNeto">
                    </div>

                    <div class="form-group col-sm-12">
                      <label for="observaciones">- OBSERVACIONES</label>
                      <textarea class="form-group" name="observaciones" id="observaciones" cols="145" rows="4" style="width: 100%;"></textarea>
                    </div>

                  </div>

                  <button type="submit" class="btn btn-block bg-gradient-primary" style="width: 200px;">Enviar</button><br>
                </form>
              </div>
              <!-- /.card -->

              <!-- /.card -->
            </div>
            <!--/.col (right) -->
          </div>
          <!-- /.row -->
      </div><!-- /.container-fluid -->
      </section>

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <strong>Copyright &copy; <?php date_default_timezone_set('America/Bogota');
                                echo date('Y'); ?> <a target="_blank" href="https://peoplemarketing.com">People Marketing SAS</a>.</strong>
      All rights reserved.

    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- Select2 -->
    <script src="../../../DESIGN/assets/plugins/select2/js/select2.full.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../../../DESIGN/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="../../../DESIGN/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="../../../DESIGN/assets/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="../../../DESIGN/assets/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="../../../DESIGN/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../../DESIGN/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../../../DESIGN/assets/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../../../DESIGN/assets/plugins/moment/moment.min.js"></script>
    <script src="../../../DESIGN/assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../../../DESIGN/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="../../../DESIGN/assets/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="../../../DESIGN/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/demo.js"></script>
    <!-- Validaciones del formulario -->
    <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/form_suscrip.js"></script>
    <script>
      $(function() {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
          theme: 'bootstrap4'
        })

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
          'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
          'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservationdate').datetimepicker({
          format: 'L'
        });
        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          locale: {
            format: 'MM/DD/YYYY hh:mm A'
          }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
          },
          function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
          }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
          format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
          $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $("input[data-bootstrap-switch]").each(function() {
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

      })
    </script>

    <script>
      // JavaScript Document
      $(document).ready(function() {

        // Suscriptor Alterno
        $("#direccionAlternaSi").on("click", function() {
          $('#target').show(); //muestro mediante id
          $('.target').show(); //muestro mediante clase
        });
        $("#direccionAlternaNo").on("click", function() {
          $('#target').hide(); //oculto mediante id
          $('.target').hide(); //muestro mediante clase
        });

        //Cuenta corriente / Debito automatico

        $("#cuentaCorriente").on("click", function() {
          $('#targetCorriente').show(); //muestro mediante id
          $('.targetCorriente').show(); //muestro mediante clase
        });
        $("#targetaCredito").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });
        $("#otros-pagos").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });
        $("#codensaSi").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });
        $("#cuentaAhorros").on("click", function() {
          $('#targetCorriente').hide(); //oculto mediante id
          $('.targetCorriente').hide(); //muestro mediante clase
        });

        //Cuenta ahorros / Debito automatico
        $("#cuentaAhorros").on("click", function() {
          $('#targetAhorros').show(); //muestro mediante id
          $('.targetAhorros').show(); //muestro mediante clase
        });
        $("#targetaCredito").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });
        $("#otros-pagos").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });
        $("#codensaSi").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });
        $("#cuentaCorriente").on("click", function() {
          $('#targetAhorros').hide(); //oculto mediante id
          $('.targetAhorros').hide(); //muestro mediante clase
        });

        //Mismo suscriptor
        $("#mismoSuscripNo").on("click", function() {
          $('#targetSuscrip').show(); //muestro mediante id
          $('.targetSuscrip').show(); //muestro mediante clase
        });
        $("#mismoSuscripSi").on("click", function() {
          $('#targetSuscrip').hide(); //oculto mediante id
          $('.targetSuscrip').hide(); //muestro mediante clase
        });

        // Tipo de tarjeta
        $("#otrotargeta").on("click", function() {
          $('#cualTargeta').show(); //muestro mediante id
          $('.cualTargeta').show(); //muestro mediante clase
        });
        $("#visa").on("click", function() {
          $('#cualTargeta').hide(); //oculto mediante id
          $('.cualTargeta').hide(); //muestro mediante clase
        });
        $("#mastercard").on("click", function() {
          $('#cualTargeta').hide(); //oculto mediante id
          $('.cualTargeta').hide(); //muestro mediante clase
        });
        $("#dinners").on("click", function() {
          $('#cualTargeta').hide(); //oculto mediante id
          $('.cualTargeta').hide(); //muestro mediante clase
        });
        $("#credencial").on("click", function() {
          $('#cualTargeta').hide(); //oculto mediante id
          $('.cualTargeta').hide(); //muestro mediante clase
        });
        $("#ae").on("click", function() {
          $('#cualTargeta').hide(); //oculto mediante id
          $('.cualTargeta').hide(); //muestro mediante clase
        });

        // Tipo de pago
        $("#targetaCredito").on("click", function() {
          $('#ocultar').show(); //muestro mediante id
          $('.ocultar').show(); //muestro mediante clase
        });
        $("#codensaSi").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#cuentaCorriente").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#cuentaAhorros").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#otros-pagos").on("click", function() {
          $('#ocultar').hide(); //oculto mediante id
          $('.ocultar').hide(); //muestro mediante clase
        });
        $("#medioPagoSi").on("click", function() {
          $('#ocultarOtro').show(); //muestro mediante id
          $('.ocultarOtro').show(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#ocultarOtro').hide(); //oculto mediante id
          $('.ocultarOtro').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#ocultarOtro').hide(); //oculto mediante id
          $('.ocultarOtro').hide(); //muestro mediante clase
        });
        $("#targetaCredito").on("click", function() {
          $('#ocultarOtro').hide(); //oculto mediante id
          $('.ocultarOtro').hide(); //muestro mediante clase
        });

        //Obsequio

        $("#obsequioSi").on("click", function() {
          $('#obsequioCual').show(); //muestro mediante id
          $('.obsequioCual').show(); //muestro mediante clase
        });
        $("#obsequioNo").on("click", function() {
          $('#obsequioCual').hide(); //oculto mediante id
          $('.obsequioCual').hide(); //muestro mediante clase
        });

        //Codensa
        $("#codensaSi").on("click", function() {
          $('#targett').show(); //muestro mediante id
          $('.targett').show(); //muestro mediante clase
        });
        $("#targetaCredito").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });
        $("#otros-pagos").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });
        $("#cuentaCorriente").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });
        $("#cuentaAhorros").on("click", function() {
          $('#targett').hide(); //oculto mediante id
          $('.targett').hide(); //muestro mediante clase
        });

        // Otros pagos
        $("#medioPagoSi").on("click", function() {
          $('#medioPagoOcultarOtro').show(); //muestro mediante id
          $('.medioPagoOcultarOtro').show(); //muestro mediante clase
        });
        $("#targetaCredito").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //oculto mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });
        $("#codensaSi").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //muestro mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });
        $("#efectivo").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //oculto mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });
        $("#cheque").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //oculto mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });
        $("#cuentaCorriente").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //oculto mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });
        $("#cuentaAhorros").on("click", function() {
          $('#medioPagoOcultarOtro').hide(); //oculto mediante id
          $('.medioPagoOcultarOtro').hide(); //muestro mediante clase
        });

      });
    </script>

  </body>

  </html>
<?php } else {
  echo 'usted no tiene permiso para esta informaci&oacute;n';
} ?>