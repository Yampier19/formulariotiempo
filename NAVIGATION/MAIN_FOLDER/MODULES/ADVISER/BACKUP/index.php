<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>EL TIEMPO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../../DESIGN/assets/bootstrap-4.5.0-dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../../../DESIGN/assets/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="../../../DESIGN/assets/bootstrap-4.5.0-dist/css/main.css">
  <script src="../../../DESIGN/assets/plugins/jquery/jquery.min.js"></script>
  <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccion.js"></script>
  <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/main.js" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      var via = $('#VIA').val();
      var dt_via = $('#detalle_via').val();
      $('#VIA').change(function() {
        dir();
      });

      $('#detalle_via').change(function() {
        dir();
      });
      $('#numero').change(function() {
        dir();
      });
      $('#numero2').change(function() {
        dir();
      });
      $('#interior').change(function() {
        dir();
      });
      $('#detalle_int').change(function() {
        dir();
      });
      $('#interior2').change(function() {
        dir();
      });
      $('#detalle_int2').change(function() {
        dir();
      });
      $('#interior3').change(function() {
        dir();
      });
      $('#detalle_int3').change(function() {
        dir();
      });

      $('#ciudad').select2();
    });
  </script>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="index.php" class="nav-link">Inicio</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>

      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="../../../DESIGN/IMG/eltiempo_logo (1).png" alt="AdminLTE Logo" class="brand-image  elevation-3" style="opacity: .8; margin-left:-4px">
         <img src="../../../DESIGN/IMG/Logo People Blanco.png" width="35%">&nbsp;&nbsp;
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="../../../DESIGN/IMG/favicon.ico" class="elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">Usuario Prueba</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item ">
              <a href="#" class="nav-link active">
                <i class="nav-icon far fa-envelope"></i>
                <p>
                  Subscripcion
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>

            <li class="nav-item">
              <a href="pages/mailbox/mailbox.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Mis Subscripciones</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/mailbox/compose.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Perfil</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/mailbox/read-mail.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Read</p>
              </a>
            </li>

            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark"></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active">Dashboard v1</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-sm-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="col-sm-12">

                </div>
              </div>
              <hr class="linea">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="../../../FUNCTIONS/CRUD/form_suscripcion.php" method="post" enctype="multipart/form-data" name="form" id="form">
                <div class="form-row">
                  <div class="form-group col-sm-8">
                    <label class="form-check-label" for="suscripcion"><b>A. SUSCRIPCI&Oacute;N</b></label>
                    - NUEVA <input class="form-group" type="radio" name="suscripcion" id="nueva" value="nueva">
                    - RENOVACI&Oacute;N <input class="form-group" type="radio" name="suscripcion" id="renovacion" value="renovacion">
                  </div>

                  <div class="form-group col-sm-3">
                    <label for="fechaventa"><b>FECHA DE VENTA</b></label>
                    <input type="date" class="form-control" id="fechaVenta" name="fechaVenta">
                  </div>

                  

                  <div class="form-group col-sm-8">
                    <label for="publicacion"> <b>B. PUBLICACI&Oacute;N</b></label><br>
                    - EL TIEMPO <input class="form-group" type="radio" name="publicacion" id="eltiempo" value="EL TIEMPO">
                    - EL TIEMPO DG PLATINO <input class="form-group" type="radio" name="publicacion" id="eltiempoGgPlatino" value="EL TIEMPO DG PLATINO">
                    - EL TIEMPO DG ORO <input class="form-group" type="radio" name="publicacion" id="eltiempoDgOro" value="EL TIEMPO DG ORO">
                    - PORTAFOLIO <input class="form-group" type="radio" name="publicacion" id="portafolio" value="PORTAFOLIO"><br>
                    - REVISTA PORTAFOLIO <input class="form-group" type="radio" name="publicacion" id="revistaPortafolio" value="REVISTA PORTAFOLIO">
                    - REVISTA Al&Oacute; <input class="form-group" type="radio" name="publicacion" id="revistaAlo" value="REVISTA AlO">
                    - REVISTA DON JUAN <input class="form-group" type="radio" name="publicacion" id="revistaDonJuan" value="REVISTA DON JUAN">
                    - REVISTA HABITAR <input class="form-group" type="radio" name="publicacion" id="revistaHabitar" value="REVISTA HABITAR"><br>
                    - REVISTA BOCAS <input class="form-group" type="radio" name="publicacion" id="revistaBocas" value="REVISTA BOCAS">
                    - OTRO <input class="form-group " type="radio" name="publicacion" id="otroPublicacion" value="OTRO">
                    <input class=" form-control otraOpcion" type="text" name="publicacionOt" id="cualOtro" style="display: none;">
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="fechaInicioSuscrip">FECHA DE INICIO DE LA SUSCRIPCI&Oacute;N</label>
                    <input type="date" class="form-control" id="fechaInicioSuscrip" name="fechaInicioSuscrip">
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="periodoSuscrip">C. PERIODO DE LA SUSCRIPCI&Oacute;N</label><br>
                    - 1 MES <input class="form-group" type="radio" name="periodoSuscrip" id="unMes" value="1 MES">
                    - 3 MESES <input class="form-group" type="radio" name="periodoSuscrip" id="tresMeses" value="3 MESES">
                    - 6 MESES <input class="form-group" type="radio" name="periodoSuscrip" id="seisMeses" value="6 MESES">
                    - 12 MESES <input class="form-group" type="radio" name="periodoSuscrip" id="doceMeses" value="12 MESES"><br>
                    - OTRO <input class="form-group" type="radio" name="periodoSuscrip" id="otroSuscrip" value="OTRO">
                    <input class="form-control cualSuscrip ocultarSuscrip" type="text" name="periodoSuscripOt" id="cualSuscrip ocultarSuscrip" style="display: none;">
                  </div>

                  <div class="form-group col-sm-7">
                    <label for="frecuencia">D. FRECUENCIA</label><br>
                    - Todos los d&iacute;as <input class="form-group" type="radio" name="frecuencia" id="todoslosdias" value="TODOS LOS DIAS">
                    - Lunes <input class="form-group" type="radio" name="frecuencia" id="lunes" value="LUNES">
                    - Martes <input class="form-group" type="radio" name="frecuencia" id="martes" value="MARTES">
                    - Miercoles <input class="form-group" type="radio" name="frecuencia" id="miercoles" value="MIERCOLES">
                    - Jueves <input class="form-group" type="radio" name="frecuencia" id="jueves" value="JUEVES">
                    - Viernes <input class="form-group" type="radio" name="frecuencia" id="viernes" value="VIERNES">
                    - S&aacute;bado <input class="form-group" type="radio" name="frecuencia" id="sabado" value="SABADO"><br>
                    - Domingo <input class="form-group" type="radio" name="frecuencia" id="domingo" value="DOMINGO">
                    - Lunes a S&aacute;bado <input class="form-group" type="radio" name="frecuencia" id="LUNES A SABADO" value="lunesSabado">
                    - Quincenal<input class="form-group" type="radio" name="frecuencia" id="quincenal" value="QUINCENAL">
                    - Mensual <input class="form-group" type="radio" name="frecuencia" id="mensual" value="MENSUAL">
                    - Bimensual <input class="form-group" type="radio" name="frecuencia" id="BIMENSUAL" value="bimensual">
                    - OTRO <input class="form-group" type="radio" name="frecuencia" id="otraFrecuencia" value="OTRO">
                    <input class="form-control cualFrecuencia" type="text" name="frecuenciaOt" id="cualFrecuencia" style="display: none;">
                  </div>

                  <div class="form-group col-sm-12">
                    <label for="informacioncliente" class="blue">E. INFORMACI&Oacute;N DEL CLIENTE / PAGADOR</label><br>
                  </div>


                  <div class="form-group col-sm-3">
                    <label for="tipoPersona"></label>
                    <b>- Persona N.</b> <input class="form-group" type="radio" name="tipoPersona" id="personanatural" value="personanatural">
                    <b>- P. Jur&iacute;dica</b> <input class="form-group" type="radio" name="tipoPersona" id="personajuridica" value="personajuridica">
                  </div>


                  <div class="form-group col-sm-3">
                    <label for="tipoDocumento">TIPO DE DOCUMENTO</label><br>
                    - C.C <input class="form-group" type="radio" name="tipoDocumento" id="cc" value="cc">
                    - C.E <input class="form-group" type="radio" name="tipoDocumento" id="ce" value="ce">
                    - T.I <input class="form-group" type="radio" name="tipoDocumento" id="ti" value="ti">
                    - NIT. <input class="form-group" type="radio" name="tipoDocumento" id="nit" value="nit">
                  </div>


              
                  <div class="form-group col-sm-2">
                    <div class="row">
                      <label for="">No.</label>
                      <div class="input-group ">
                        <div class="input-group-prepend numeroDocumentoCliente">
                          <span class="input-group-text" id="basic-addon1"><i class="far fa-address-card"></i></span>
                        </div>
                        <input type="number" class="form-control" name="numeroDocumentoCliente" id="numeroDocumentoCliente" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
                      </div>
                    </div>
                  </div>




                  <div class="form-group col-sm-4">
                    <label for="medioContacto">MEDIO PREFERIDO PARA SER CONTACTADO </label><br>
                    - E-MAIL <input class="form-group" type="radio" name="medioContacto" id="emailContacto" value="emailContacto">
                    - CELULAR <input class="form-group" type="radio" name="medioContacto" id="celularContacto" value="celularContacto">
                    - TEL. CASA <input class="form-group" type="radio" name="medioContacto" id="telCasaContacto" value="telCasaContacto">
                    - TEL. OFICINA <input class="form-group" type="radio" name="medioContacto" id="telContactoOficina" value="telContactoOficina">
                  </div>


                  <div class="form-group col-sm-12">
                    <div class="row">
                      <label for="">NOMBRES COMPLETOS Y/O RAZ&Oacute;N SOCIAL</label>
                      <div class="input-group ">
                        <div class="input-group-prepend nombreCompletoCliente">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
                        </div>
                        <input type="text" class="form-control nombreCompletoCliente" name="nombreCompletoCliente" id="nombreCompletoCliente" placeholder="" aria-label="nombreCompletoCliente" aria-describedby= "basic-addon1">
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-sm-2">
                    <div class="row">
                      <label for="">TEL&Eacute;FONO CASA</label>
                      <div class="input-group ">
                        <div class="input-group-prepend telCasa">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-house-user"></i></i></span>
                        </div>
                        <input type="number" class="form-control telCasa" name="telCasa" id="telCasa" placeholder="" aria-label="telCasa" aria-describedby="basic-addon1">
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-sm-2">
                    <div class="row">
                      <label for="">TEL&Eacute;FONO OFICINA</label>
                      <div class="input-group ">
                        <div class="input-group-prepend telOficina">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-volume"></i></i></span>
                        </div>
                        <input type="number" class="form-control telOficina" name="telOficina" id="telOficina" placeholder="" aria-label="telOficina" aria-describedby="basic-addon1">
                      </div>
                    </div>
                  </div>


                  <div class="form-group col-sm-2">
                    <label for="extension">EXT</label><br>
                    <input class="form-control" type="number" name="extension" id="extension">
                  </div>

                  <div class="form-group col-sm-2">
                    <div class="row">
                      <label for="">CELULAR</label>
                      <div class="input-group ">
                        <div class="input-group-prepend celular">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                        </div>
                        <input type="number" class="form-control celular" name="celular" id="celular" placeholder="" aria-label="celular" aria-describedby="basic-addon1">
                      </div>
                    </div>
                  </div>


                  <div class="form-group col-sm-3">
                    <label for="fechaNacimiento">FECHA DE NACIMIENTO</label>
                    <input type="date" class="form-control" id="fechaNacimiento">
                  </div>

                  <div class="form-group col-sm-6">
                    <div class="row">
                      <label for="">Correo Electr&oacute;nico</label>
                      <div class="input-group ">
                        <div class="input-group-prepend email">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
                        </div>
                        <input type="email" class="form-control email" name="celular" id="email" placeholder="" aria-label="email" aria-describedby="basic-addon1">
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="estadoCivil">ESTADO CIVIL</label><br>
                    - SOLTERO <input class="form-group" type="radio" name="estadoCivil" id="soltero" value="soltero">
                    - CASADO <input class="form-group" type="radio" name="estadoCivil" id="casado" value="casado">
                    - VIUDO <input class="form-group" type="radio" name="estadoCivil" id="viudo" value="viudo">
                    - UNI&Oacute;N LIBRE<input class="form-group" type="radio" name="estadoCivil" id="unionlibre" value="unionlibre">
                  </div>


                  <div class="form-group col-sm-12 blue">
                    <label>F. INFORMACI&Oacute;N DEL SUSCRITOR / TITULAR</label><br>
                  </div>

                  <div class="form-group col-sm-3">
                    <label for="tipoSuscriptor"><b><u>EL PAGADOR ES EL MISMO SUSCRIPTOR O ES
                          EMPRESA:</u></b></label><br>
                    - SI <input class="form-group" type="radio" name="tipoSuscriptor" id="mismoSuscripSi" value="si">
                    - NO <input class="form-group" type="radio" name="tipoSuscriptor" id="mismoSuscripNo" value="no">
                  </div>

                  <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label><b><u>DILIGENCIAR LO SIGUIENTE SI CONTESTO NO</u></b></label>
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="nit">NIT</label><br>
                    <input class="form-control" type="number" name="nit" id="nit">
                  </div>

                  <div class="form-group col-sm-4 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="medioContactoAlterno">MEDIO PREFERIDO PARA SER CONTACTADO</label><br>
                    E-MAIL <input class="form-group" type="radio" name="medioContactoAlterno" id="emailAlterno" value="emailAlterno">
                    CELULAR <input class="form-group" type="radio" name="medioContactoAlterno" id="celularAlterno" value="celularAlterno">
                    TEL. CASA <input class="form-group" type="radio" name="medioContactoAlterno" id="telCasaAlterno" value="telCasaAlterno">
                    TEL. OFICINA <input class="form-group" type="radio" name="medioContactoAlterno" id="telOficinaAlterno" value="telOficinaAlterno">
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="tipoDocumentoSuscrip">TIPO DE DOCUMENTO</label><br>
                    - C.C <input class="form-group" type="radio" name="tipoDocumentoSuscrip" id="ccAlterna" value="cc">
                    - C.E <input class="form-group" type="radio" name="tipoDocumentoSuscrip" id="ceAlterna" value="ce">
                    - T.I <input class="form-group" type="radio" name="tipoDocumentoSuscrip" id="tiAlterno" value="ti"><br>
                    - NIT. <input class="form-group" type="radio" name="tipoDocumentoSuscrip" id="nitAlterno" value="nit">
                  </div>

                  <div class="form-group col-lg-1 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="genero">G&Eacute;NERO</label><br>
                    - M <input class="form-group" type="radio" name="genero" id="masculino" value="masculino">
                    - F <input class="form-group" type="radio" name="genero" id="femenino" value="femenino">
                  </div>

                  <div class="form-group col-sm-9 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="nombresCompletosSuscrip">NOMBRES COMPLETOS Y/O RAZ&Oacute;N SOCIAL</label><br>
                    <input class="form-control" type="text" name="nombresCompletosSuscrip" id="nombreCompletoAlterno">
                  </div>

                  <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="noDocumentoSuscriptor">No.</label>
                    <input class="form-control" type="number" name="noDocumentoSuscriptor" id="noDocumentoAlterno">
                  </div>

                  <div class="form-group col-sm-9 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="apellidosSuscrip">APELLIDOS</label><br>
                    <input class="form-control" type="text" name="apellidosSuscrip" id="apellidosAlternos">
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="telCasaSuscrip">TEL&Eacute;FONO CASA</label><br>
                    <input class="form-control" type="number" name="telCasaSuscrip" id="telCasaAlterno">
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="telOficinaSuscrip">TEL&Eacute;FONO OFICINA</label><br>
                    <input class="form-control" type="number" name="telOficinaSuscrip" id="telOficinaAlterno">
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="extensionSuscrip">EXT</label><br>
                    <input class="form-control" type="number" name="extensionSuscrip" id="extensionAlterna">
                  </div>

                  <div class="form-group col-sm-2 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="celularSuscrip">CELULAR</label><br>
                    <input class="form-control" type="number" name="celularSuscrip" id="celularSuscripAlterno">
                  </div>

                  <div class="form-group col-sm-3 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="fechaNacimientoSuscrip">FECHA DE NACIMIENTO</label>
                    <input type="date" class="form-control" id="fechaNacimientoSuscrip">
                  </div>

                  <div class="form-group col-sm-6 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="emailsuscrip">Correo Electr&oacute;nico</label>
                    <input type="email" class="form-control" id="emailsuscripalterno" name="emailsuscrip">
                  </div>

                  <div class="form-group col-sm-6 targetSuscrip" id="targetSuscrip" style="display: none;">
                    <label for="estadoCivilSuscrip">ESTADO CIVIL</label><br>
                    - SOLTERO <input class="form-group" type="radio" name="estadoCivilSuscrip" id="solteroAlterno" value="soltero">
                    - CASADO <input class="form-group" type="radio" name="estadoCivilSuscrip" id="casadoAlterno" value="casado">
                    - VIUDO <input class="form-group" type="radio" name="estadoCivilSuscrip" id="viudoAlterno" value="viudo">
                    - UNI&Oacute;N LIBRE <input class="form-group" type="radio" name="estadoCivilSuscrip" id="unionlibreAlterno" value="unionlibre">
                  </div>

                  <div class="form-group col-sm-12 blue">
                    <label for="direccionSuscripcion"><b>DIRECCI&Oacute;N DE ENTREGA DE LA SUSCRIPCI&Oacute;N</b></label>
                  </div>
                  <div class="col-md-12 col-sm-12">
                    <input type="text" class="form-control has-feedback-left" placeholder="Direcci&oacute;n" name="DIRECCION" id="DIRECCION" readonly="readonly" />

                  </div><br>

                  <br>
                  <div class="col-sm-2">Via:
                    <select id="VIA" name="VIA" class="form-control">
                      <option value="">Seleccione...</option>
                      <option>ANILLO VIAL</option>
                      <option>AUTOPISTA</option>
                      <option>AVENIDA</option>
                      <option>BOULEVAR</option>
                      <option>CALLE</option>
                      <option>CALLEJON</option>
                      <option>CARRERA</option>
                      <option>CIRCUNVALAR</option>
                      <option>CONDOMINIO</option>
                      <option>DIAGONAL</option>
                      <option>KILOMETRO</option>
                      <option>LOTE</option>
                      <option>SALIDA</option>
                      <option>SECTOR</option>
                      <option>TRANSVERSAL</option>
                      <option>VEREDA</option>
                      <option>VIA</option>
                    </select>
                  </div>

                  <div class="col-sm-3 ">Detalles Via:
                    <input name="detalle_via" id="detalle_via" type="text" maxlength="60" class="form-control" />
                  </div>




                  <div class="form-group col-md-3">N&uacute;mero:
                    <input name="numero" id="numero" type="text" maxlength="15" class="form-control" />
                  </div>

                  <div class="form-group col-md-2">Extensi&oacute;n

                    <input name="numero2" id="numero2" type="text" maxlength="15" class="form-control" />
                  </div>


                  <br>
                  <div class="row col-md-12 col-sm-12 ">
                    <div class="col-md-2 col-sm-2 ">Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <select id="interior" name="interior" class="form-control">
                        <option value="">Seleccione...</option>
                        <option>APARTAMENTO</option>
                        <option>BARRIO</option>
                        <option>BLOQUE</option>
                        <option>CASA</option>
                        <option>CIUDADELA</option>
                        <option>CONJUNTO</option>
                        <option>CONJUNTO RESIDENCIAL</option>
                        <option>EDIFICIO</option>
                        <option>UNIDAD</option>
                        <option>ENTRADA</option>
                        <option>ESTE</option>
                        <option>ETAPA</option>
                        <option>INTERIOR</option>
                        <option>MANZANA</option>
                        <option>NORTE</option>
                        <option>OESTE</option>
                        <option>OFICINA</option>
                        <option>OCCIDENTE</option>
                        <option>ORIENTE</option>
                        <option>PENTHOUSE</option>
                        <option>PISO</option>
                        <option>PORTERIA</option>
                        <option>SOTANO</option>
                        <option>SUR</option>
                        <option>TORRE</option>
                      </select>
                    </div>
                    <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <input name="detalle_int" id="detalle_int" type="text" maxlength="30" class="form-control" />
                    </div>
                  </div>

                  <div class="col-md-12 col-sm-12 "></div>

                  <br>
                  <div class="row col-md-12 col-sm-12 ">
                    <div class="col-md-2 col-sm-2 ">Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <select id="interior2" name="interior2" class="form-control">
                        <option value="">Seleccione...</option>
                        <option>APARTAMENTO</option>
                        <option>BARRIO</option>
                        <option>BLOQUE</option>
                        <option>CASA</option>
                        <option>CIUDADELA</option>
                        <option>CONJUNTO</option>
                        <option>CONJUNTO RESIDENCIAL</option>
                        <option>EDIFICIO</option>
                        <option>UNIDAD</option>
                        <option>ENTRADA</option>
                        <option>ESTE</option>
                        <option>ETAPA</option>
                        <option>INTERIOR</option>
                        <option>MANZANA</option>
                        <option>NORTE</option>
                        <option>OESTE</option>
                        <option>OFICINA</option>
                        <option>OCCIDENTE</option>
                        <option>ORIENTE</option>
                        <option>PENTHOUSE</option>
                        <option>PISO</option>
                        <option>PORTERIA</option>
                        <option>SOTANO</option>
                        <option>SUR</option>
                        <option>TORRE</option>
                      </select>
                    </div>
                    <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" class="form-control" />
                    </div>
                  </div>

                  <div class="col-md-12 col-sm-12 "></div>

                  <br>
                  <div class="row col-md-12 col-sm-12 ">
                    <div class="col-md-2 col-sm-2 ">Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <select id="interior3" name="interior3" class="form-control">
                        <option value="">Seleccione...</option>
                        <option>APARTAMENTO</option>
                        <option>BARRIO</option>
                        <option>BLOQUE</option>
                        <option>CASA</option>
                        <option>CIUDADELA</option>
                        <option>CONJUNTO</option>
                        <option>CONJUNTO RESIDENCIAL</option>
                        <option>EDIFICIO</option>
                        <option>UNIDAD</option>
                        <option>ENTRADA</option>
                        <option>ESTE</option>
                        <option>ETAPA</option>
                        <option>INTERIOR</option>
                        <option>MANZANA</option>
                        <option>NORTE</option>
                        <option>OESTE</option>
                        <option>OFICINA</option>
                        <option>OCCIDENTE</option>
                        <option>ORIENTE</option>
                        <option>PENTHOUSE</option>
                        <option>PISO</option>
                        <option>PORTERIA</option>
                        <option>SOTANO</option>
                        <option>SUR</option>
                        <option>TORRE</option>
                      </select>
                    </div>
                    <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                    <div class="col-md-4 col-sm-4 ">
                      <input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" class="form-control" />
                    </div>
                  </div><br>

                  <br><br>
                  <div class="form-group col-sm-12">
                    <label for="direccionAlterna">DIRECCI&Oacute;N ALTERNA DE ENTREGA DE LA SUSCRIPCI&Oacute;N: </label>
                    SI <input type="radio" class="form-group" name="direccionAlterna" id="direccionAlternaSi" value="si">
                    NO <input type="radio" class="form-group" name="direccionAlterna" id="direccionAlternaNo" value="no">
                  </div>



                  <div class="form-group col-sm-12 target" id="target" style="display: none;">
                    <label for="estadocivil"><b><u>DIRECCI&Oacute;N ALTERNA DE ENTREGA DE LA SUSCRIPCI&Oacute;N</u></b></label>


                    <div class="form-group col-sm-12 target" id="target" style="display: none;">
                      <label for="direccionAlterna"><b>DIRECCI&Oacute;N</b></label>
                      <input class="form-control" type="text" name="direccionAlterna" id="direccionAlterna">
                    </div>

                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="viaPrincipalAlterna"><b>V&Iacute;A PRINCIPAL</b></label><br>
                    <select class="form-control" name="viaPrincipalAlterna" id="viaPrincipalAlterna"><b>V&Iacute;A
                        PRINCIPAL</b>
                      <option value=""></option>
                      <option value="avenidaalterna">AVENIDA</option>
                      <option value="akalterna">AK</option>
                      <option value="acalterna">AC</option>
                      <option value="viaalterna">V&Iacute;A</option>
                      <option value="callealterna">CALLE</option>
                      <option value="carreraalterna">CARRERA</option>
                      <option value="diagonalalterna">DIAGONAL</option>
                      <option value="transversalalterna">TRANSVERSAL</option>
                      <option value="circularalterna">CIRCULAR</option>
                      <option value="otroalternrna">OTRO</option>
                    </select>
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="posfijoAlterno"><b>POSFIJO</b></label><br>
                    <select class="form-control" name="posfijoAlterno" id="posfijoAlterno">
                      <option value=""></option>
                      <option value="suralterna">SUR</option>
                      <option value="estealterna">ESTE</option>
                      <option value="nortealterna">NORTE</option>
                      <option value="oestealterna">OESTE</option>
                      <option value="suroestealterna">SUROESTE</option>
                    </select>
                  </div>

                  <div class="form-group col-sm-4 target" id="target" style="display: none;">
                    <label for="numeroViaPrincipalAlterna">N&uacute;mero o nombre de la v&iacute;a principal:</label><br>
                    <input class="form-control" type="text" name="numeroViaPrincipalAlterna" id="numeroViaPrincipalAlterna">
                  </div>

                  <div class="form-group col-sm-4 target" id="target" style="display: none;">
                    <label for="cruceViaAlterna">N&uacute;mero y/o letra del cruce de la v&iacute;a:</label><br>
                    <input class="form-control" type="text" name="cruceViaAlterna" id="cruceViaAlterna">
                  </div>

                  <div class="form-group col-sm-4 target" id="target" style="display: none;">
                    <label for="numeroPlacaAlterna">N&uacute;mero de placa:</label><br>
                    <input class="form-control" type="text" name="numeroPlacaAlterna" id="numeroPlacaAlterna">
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="otrosComplementosAlternos"><b>COMPLEMENTO</b></label>
                    <select class="form-control" name="otrosComplementosAlternos" id="otrosComplementosAlternos">
                      <option value=""></option>
                      <option value="bloquealterno">Bloque</option>
                      <option value="celulalterno">C&eacute;lula</option>
                      <option value="etapaalterno">Etapa</option>
                      <option value="manzanaalterno">Manzana</option>
                      <option value="mezzaninealterno">Mezzanine</option>
                      <option value="moduloalterno">M&oacute;dulo</option>
                      <option value="sectoralterno">Sector</option>
                    </select>
                  </div>


                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="noComplementoAlterno">No.</label><br>
                    <input class="form-control" type="text" name="noComplementoAlterno" id="noComplementoAlterno">
                  </div>


                  <div class="form-group col-sm-12 target" id="target" style="display: none;">
                    <label for="interior"><b>INTERIOR</b></label>
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="tipoviviendaalterno">Vivienda</label>
                    <select class="form-control" name="tipoviviendaalterno" id="tipoviviendaalterno">
                      <option value=""></option>
                      <option value="aptoalterno">Apto</option>
                      <option value="casaalterno">Casa</option>
                    </select>
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="noInteriorAlterno">No.</label>
                    <input class="form-control" type="number" name="noInteriorAlterno" id="noInteriorAlterno">
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="veredaAlterna">Vereda</label>
                    <input class="form-control" type="text" name="veredaAlterna" id="veredaAlterna">
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="departamentoAlterno">Departamento</label>
                    <input class="form-control" type="text" name="departamentoAlterno" id="departamentoAlterno">
                  </div>


                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="urbanizacionAlterna">Urbanizaci&oacute;n</label>
                    <input class="form-control" type="text" name="urbanizacionAlterna" id="urbanizacionAlterna">
                  </div>


                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="ciudadAlterna">Ciudad</label>
                    <input class="form-control" type="text" name="ciudadAlterna" id="ciudadAlterna">
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="conjuntoAlterno">Conjunto</label>
                    <input class="form-control" type="text" name="conjuntoAlterno" id="conjuntoAlterno">
                  </div>


                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="barrioAlterno">Barrio</label>
                    <input class="form-control" type="text" name="barrioAlterno" id="barrioAlterno">
                  </div>

                  <div class="form-group col-sm-6 target" id="target" style="display: none;">
                    <label for="edificioAlterno">Edificio</label>
                    <input class="form-control" type="text" name="edificioAlterno" id="edificioAlterno">
                  </div>


                  <div class="form-group col-sm-12">
                    <p>El Suscriptor debera entregar a CEET sus datos personales y de contacto con el fin de poder
                      realizar la entrega del producto correctamente (direcci&oacute;n actual municipio, ciudad, telefonos,
                      c&eacute;dula, NIT, entre otras.) El suscriptor sera el unico responsable por la exactitud y veracidad
                      de
                      los datos que entregue o registre, declarando que mantendra indemne a CEET por cualquier novedad
                      que ocurra en la prestaci&oacute;n del servicio que se origine porta faita de exactitud o veracidad de
                      los datos entregados.</p>
                  </div>


                  <div class="form-group col-sm-12 blue">
                    <label>G. FORMA DE PAGO</label><br>
                  </div>



                  <div class="form-group col-sm-3">
                    <label for="medioPago">- TARGETA DE CR&Eacute;DITO </label>
                    <input class="form-group" type="radio" name="medioPago" id="targetaCredito" value="targetaCredito">
                  </div>

                  <div class="form-group col-sm-6 ocultar" id="ocultar" style="display: none;">
                    <label for="tipoTargeta"><b>TIPO</b></label><br>
                    - VISA <input class="form-group" type="radio" name="tipoTargeta" id="visa" value="visa">
                    - MASTER CARD <input class="form-group" type="radio" name="tipoTargeta" id="mastercard" value="mastercard">
                    - DINERS <input class="form-group" type="radio" name="tipoTargeta" id="dinners" value="dinners">
                    - CREDENCIAL <input class="form-group" type="radio" name="tipoTargeta" id="credencial" value="credencia">
                    - A.E <input class="form-group" type="radio" name="tipoTargeta" id="ae" value="ae">
                    - OTRO <input class="form-group" type="radio" name="tipoTargeta" id="otrotargeta" value="otrotargetas"><br>
                    <input class=" form-control cualTargeta" type="text" name="tipoTargeta" id="cualTargeta" style="display: none;">
                  </div>

                  <div class="form-group col-sm-3 ocultar" id="ocultar" style="display: none;">
                    <label for="noTargeta">No.</label><br>
                    <input class="form-control" type="number" name="noTargeta" id="noTargeta">
                  </div>

                  <div class="form-group col-sm-3 ocultar" id="ocultar" style="display: none;">
                    <label for="banco">BANCO</label>
                    <input class=" form-control" type="text" name="banco" id="banco">
                  </div>

                  <div class="form-group col-sm-3 ocultar" id="ocultar" style="display: none;">
                    <label for="noCuotas">No. CUOTAS</label>
                    <input class=" form-control" type="number" name="noCuotas" id="noCuotas">
                  </div>



                  <div class="form-group col-sm-3 ocultar" id="expirationMes ocultar" style="display: none;">
                    <label for="fechaventa"><b>VENCIMIENTO:</b></label>
                    <input type="month" class="form-control" id="month">
                  </div>


                  <div class="form-group col-sm-3 ocultar" id="ocultar" style="display: none;">
                    <label for="codensa">- CODENSA</label><br>
                    Si <input class="form-group" type="radio" name="codensa" id="codensaSi" value="si">
                    No <input class="form-group" type="radio" name="codensa" id="codensaNo" value="no">
                  </div>

                  <div class="form-group col-sm-2 ocultar" id="ocultar" style="display: none;">
                    <label for="nie">- NIE</label>
                    <input class=" form-control" type="number" name="nie" id="nie">
                  </div>


                  <hr class="linea">


                  <div class="form-group col-sm-3">
                    <label for="medioPago">- EFECTIVO </label>
                    <input class="form-group" type="radio" name="medioPago" id="efectivo" value="efectivo">
                  </div>

                  <div class="form-group col-sm-3">
                    <label for="medioPago">- CHEQUE</label>
                    <input class="form-group" type="radio" name="medioPago" id="cheque" value="cheque">
                  </div>

                  <div class="form-group col-sm-3 otros-pagos" id="otros-pagos">
                    <label for="medioPago">- OTRAS FORMAS DE PAGO </label>
                    <input class="form-group" type="radio" name="medioPago" id="medioPagoSi" value="si">

                    <br>
                    <input class=" form-control ocultarOtro" type="text" name="medioPago" id="medioPago ocultarOtro" style="display: none;">
                  </div>

                  <div class="form-group col-sm-2">
                    <label for="cuentaCorriente">- CUENTA CORRIENTE</label>
                    <input class="form-group" type="radio" name="tipoCuenta" id="cuentaCorriente" value="cuentaCorriente">
                  </div>

                  <div class="form-group col-sm-3 ">
                    <label for="cuentaAhorros">- CUENTA DE AHORROS</label>
                    <input class="form-group" type="radio" name="tipoCuenta" id="cuentaAhorros" value="cuentaAhorros">
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="debitoAutomatico">- DEBITO AUTOM&Aacute;TICO</label>
                    Si <input class="form-group" type="radio" name="debitoAutomatico" id="debitoAutomaticoSi" value="si">
                    No <input class="form-group" type="radio" name="debitoAutomatico" id="debitoAutomaticoNo" value="no">
                  </div>

                  <div class="form-group col-sm-3">
                    <label for="obsequio">- SE OFRECE OBSEQUIO </label>
                    Si <input class="form-group obsequioSi" type="radio" name="obsequio" id="obsequioSi" value="si">
                    No <input class="form-group obsequioNo" type="radio" name="obsequio" id="obsequioNo" value="no"> <br>
                    <input class=" form-control obsequioCual" type="text" name="obsequio" id="obsequioCual" style="display: none;">
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="precioBruto">- PRECIO BRUTO</label>
                    <input class=" form-control" type="number" name="precioBruto" id="precioBruto">
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="descuento">- DESCUENTO</label>
                    <input class=" form-control" type="number" name="descuento" id="descuento">
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="valorNeto">- VALOR NETO</label>
                    <input class=" form-control" type="number" name="valorNeto" id="valorNeto">
                  </div>

                  <div class="form-group col-sm-12">
                    <label for="observaciones">- OBSERVACIONES</label>
                    <textarea class="form-group" name="observaciones" id="observaciones" cols="145" rows="4" style="width: 100%;"></textarea>
                  </div>

                </div>

                <button type="submit" class="btn btn-block bg-gradient-primary" style="width: 200px;">Enviar</button><br>
              </form>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?php date_default_timezone_set('America/Bogota');
                              echo date('Y'); ?> <a target="_blank" href="https://peoplemarketing.com">People Marketing SAS</a>.</strong>
    All rights reserved.

  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->

  <!-- jQuery UI 1.11.4 -->
  <script src="../../../DESIGN/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="../../../DESIGN/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="../../../DESIGN/assets/plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="../../../DESIGN/assets/plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="../../../DESIGN/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="../../../DESIGN/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="../../../DESIGN/assets/plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="../../../DESIGN/assets/plugins/moment/moment.min.js"></script>
  <script src="../../../DESIGN/assets/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="../../../DESIGN/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="../../../DESIGN/assets/plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="../../../DESIGN/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../../DESIGN/assets/bootstrap-4.5.0-dist/js/demo.js"></script>
  <!-- Validaciones del formulario -->
  <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/form_suscrip.js"></script>




</body>

</html>