
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar  elevation-4" style="    background-color: #222d32;">
 <!-- Brand Logo -->
 <img src="../../../../DESIGN/IMG/eltiempo_logo (1).png" alt="AdminLTE Logo" class="brand-image w-75 ml-3 my-3">
         

      <!-- Sidebar -->
      <div class="sidebar ">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="../../../../DESIGN/IMG/favicon.ico" class="elevation-2" alt="User Image">
          </div>
          <div class="info ">
            <a href="#" class="d-block text-white">Administrador</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            
               <li class="nav-item ">

                <a href="../historysubs.php" style="color:#fff" class="nav-link <?php if(basename($_SERVER['PHP_SELF'])=="../historysubs.php"){
                  echo "active";
                }else{
                  echo "";
                } ?>">
                  <i class="nav-icon far fa-address-card"></i>
                  <p >
                    Historial suscripciones
                  </p>
                </a>
                </li>


                <li class="nav-item ">

                <a href="../reports.php" style="color:#fff" class="nav-link <?php if(basename($_SERVER['PHP_SELF'])=="../reports.php"){
                  echo "active";
                }else{
                  echo "";
                } ?>">
                  <i class="nav-icon far fa-address-card"></i>
                  <p>
                    Reportes
                  </p>
                </a>
                </li>


                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" style="color:#fff" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nav-icon far fa-copy"></i> 
                  <p>Control de Usuarios</p>
                  </a>
                  <div class="dropdown-menu bg-dark " aria-labelledby="navbarDropdown" >
                    <a class="dropdown-item <?php if(basename($_SERVER['PHP_SELF'])=="insert_user.php"){
                    echo "active";
                  }else{
                    echo "";
                  } ?>" href="insert_user.php">Agregar usuario</a>
                    <a class="dropdown-item <?php if(basename($_SERVER['PHP_SELF'])=="modify_user.php"){
                    echo "active";
                  }else{
                    echo "";
                  } ?>" href="modify_user.php">Modificar Usuario</a>
                </li>


                <li class="nav-item ">

                  <a href="../my_subscriptions.php"  style="color:#fff" class="nav-link <?php if(basename($_SERVER['PHP_SELF'])=="../my_subscriptions.php"){
                    echo "active";
                  }else{
                    echo "";
                  } ?>">
                    <i class="nav-icon far fa-address-card"></i>
                    <p>
                      Mis Suscripciones
                    </p>
                  </a>
                  </li>

            
            
            
            
                <li class="nav-item ">

                  <a href="../index.php"  style="color:#fff" class="nav-link <?php if(basename($_SERVER['PHP_SELF'])=="../index.php"){
                    echo "active";
                  }else{
                    echo "";
                  } ?>">
                    <i class="nav-icon far fa-address-card"></i>
                    <p>
                      Subscripci&oacute;n
                    </p>
                  </a>
                </li>






            <style>
                .dropdown-item:hover{
                background-color: #007bff;
              }
            </style>

           

            <li class="nav-item">
              <a href="../page_user.php" style="color:#fff"  class="nav-link <?php if(basename($_SERVER['PHP_SELF'])=="../page_user.php"){
                echo "active";
              }else{
                echo "";
              } ?>">
                <i class="nav-icon far fa-user"></i>
                <p>Perfil</p>
              </a>
            </li>
        

            </li>

          </ul>

          <img src="../../../../DESIGN/IMG/Logo People Blanco.png" alt="AdminLTE Logo" class="brand-image w-50 ml-3" style="margin-top:50%;">
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>