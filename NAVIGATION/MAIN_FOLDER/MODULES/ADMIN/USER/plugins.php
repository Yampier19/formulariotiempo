    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../../../DESIGN/assets/plugins/fontawesome-free/css/all.min.css">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <!-- iCheck -->
    <link rel="stylesheet" href="../../../../DESIGN/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">


    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="../../../../DESIGN/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../../../DESIGN/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../../../DESIGN/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <link rel="stylesheet" href="../../../../DESIGN/assets/bootstrap-4.5.0-dist/css/main.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../../DESIGN/assets/bootstrap-4.5.0-dist/css/adminlte.min.css">
    <script src="../../../../DESIGN/assets/plugins/jquery/jquery.min.js"></script>
    <script src="../../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccion.js"></script>
    <script src="../../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccionAL.js"></script>

    



    <!---------------------------------------------------------------------------------------------------------> 

  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
      
 <!--    Datatables-->
 <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script> 



  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>


     <!-- Select2 -->
     <script src="../../../../DESIGN/assets/plugins/select2/js/select2.full.min.js"></script>


    <!-- Bootstrap 4 -->
    <script src="../../../../DESIGN/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

   

    
    <!-- overlayScrollbars -->
    <script src="../../../../DESIGN/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/js/demo.js"></script>
    <!-- Validaciones del formulario -->
    <script src="../../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/form_suscrip.js"></script>


   