<?php
require('../../../../CONNECTION/SECURITY/conex.php');

require('plugins.php'); 

?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EL TIEMPO</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   


  </head>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EL TIEMPO</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    

  </head>

  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color: #222d32;">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link text-white mt-1" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index.php" class="nav-link text-white">Inicio</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link text-white">Contacto</a>
          </li>
        </ul>

      

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link text-white" data-toggle="dropdown" href="#">
              <i class="far fa-comments "></i>
              <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Brad Diesel
                      <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">Call me whenever you can...</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      John Pierce
                      <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">I got your message bro</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Nora Silvester
                      <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">The subject goes here</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link text-white" data-toggle="dropdown" href="#">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">15 Notifications</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> 4 new messages
                <span class="float-right text-muted text-sm">3 mins</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> 8 friend requests
                <span class="float-right text-muted text-sm">12 hours</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> 3 new reports
                <span class="float-right text-muted text-sm">2 days</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
          </li>

          

        </ul>
      </nav>
      <!-- /.navbar -->
      <?php include 'admin_menu.php';
      echo (basename($_SERVER['PHP_SELF']));
        ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark"></h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                  <li class="breadcrumb-item active">Editar usuario</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->



        
<?php  

$id_user = $_GET['id'];

$sql = "SELECT u.names, u.surnames, u.correo, u.documento, 
    ul.name_user, ul.activo
    FROM user u 
    INNER JOIN userlogin ul ON u.id_user = ul.id_log
    WHERE u.id_user = '$id_user'";

$query = mysqli_query($conex, $sql); 
$row = mysqli_fetch_array($query); 


//tabla datos cliente 
?> 



<div class="container col-md-10 col-sm-12 ">
    <div class="card shadow bg-white rounded">
        <div class="card-header bg-blue">
            <label for="title" class="lead">Editar Usuario</label>
        </div>
            <div class="card-body">
            <form action="metodos.php" method="post">

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-6">
                            <label for="names">Nombre</label>
                            <input type="text" name="names" id="names" class="form-control" value="<?=$row['names']?>">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="surnames">Apellido</label>
                            <input type="text" name="surnames" id="surnames" class="form-control" value="<?=$row['surnames']?>">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-6">
                            <label for="documento">Documento</label>
                            <input type="number" name="documento" id="documento" class="form-control" value="<?=$row['documento']?>">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="correo">Correo</label>
                            <input type="email" name="correo" id="correo" class="form-control" value="<?=$row['correo']?>">
                        </div>
                    </div>

                    <div class="row mb-5">
                       <div class="col-md-6 col-sm-6">
                            <label for="name_user">Nombre usuario</label>
                            <input type="text" name="name_user" id="name_user" class="form-control" value="<?=$row['name_user']?>">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="id_loginrol">Rol</label>
                            <select name="id_loginrol" id="id_loginrol" class="form-control">
                              <?php 


                              $sql = "SELECT * FROM user_rol";
                              $query = mysqli_query($conex, $sql);
                              while($row = mysqli_fetch_array($query)){
                              ?>
                                <option value="<?=$row['id_rol']?>"><?=$row['nombre_rol']?></option>
                            
                            <?php  } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-md-6 col-sm-6">
                            <label for="canal">Canal</label>
                            <select name="canal" id="canal" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="activo">Activo</label>
                            <select name="activo" id="activo" class="form-control">
                                <option selected disabled >Seleccione el rol...</option>
                                <option value="1" >Si</option>
                                <option value="0" >No</option>
                            </select>
                        </div>
                        
                    </div>

                    <div class="row mb-5">
                        <div class="col-md-12 col-sm-12">
                            <label for="password">Contraseña</label>
                            <input type="password" name="password" id="password" class="form-control">

                        </div>
                        
                    </div>

                    <input type="hidden" name="id" value="<?= $id_user ?>">

                    <div class="row mb-3">
                        <div class="col-md-4 col-sm-4 ml-3">
                        <button type="submit" id="update" name="update" class="btn btn-success btn-block">Actualizar Usuario</button>
                        </div>

                        <div class="col-md-4 col-sm-4">
                        <a href="../historysubs.php" class="btn btn-primary btn-block">Volver al inicio</a>
                        </div>
                    </div>
            </form>
            </div>
        
    </div>
</div>



        
</body>

</html>
