<?php
//fetch.php
require('../../../CONNECTION/SECURITY/conex.php');
$columns = array('id_datSuscripcion', 'suscripcion', 'publicacion', 'periodo_suscripcion', 'fecha_registro');

$query = "SELECT * FROM datos_suscripcion WHERE ";

if($_POST["is_date_search"] == "yes")
{
 $query .= 'fecha_registro BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}

if(isset($_POST["search"]["value"]))
{
 $query .= '
  (id_datSuscripcion LIKE "%'.$_POST["search"]["value"].'%" 
  OR suscripcion LIKE "%'.$_POST["search"]["value"].'%" 
  OR publicacion LIKE "%'.$_POST["search"]["value"].'%" 
  OR periodo_suscripcion LIKE "%'.$_POST["search"]["value"].'%")
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';
}
else
{
 $query .= 'ORDER BY id_datSuscripcion ASC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conex, $query));

$result = mysqli_query($conex, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = $row["id_datSuscripcion"];
 $sub_array[] = $row["suscripcion"];
 $sub_array[] = $row["publicacion"];
 $sub_array[] = $row["periodo_suscripcion"];
 $sub_array[] = $row["fecha_registro"];
 $data[] = $sub_array;
}

function get_all_data($conex)
{
 $query = "SELECT * FROM datos_suscripcion";
 $result = mysqli_query($conex, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conex),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
