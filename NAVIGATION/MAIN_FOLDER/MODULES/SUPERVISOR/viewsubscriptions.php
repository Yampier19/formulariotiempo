<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../PLUGIN/plugins.php'); 
require('../../../CONNECTION/SECURITY/session_cookie.php');

if ($id_user != ''  && $id_user != '0') {
  $id_userD = base64_decode($id_user); 
?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EL TIEMPO</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    


    <head>

    <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color: #222d32;">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link text-white mt-1" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index.php" class="nav-link text-white">Inicio</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link text-white">Contacto</a>
          </li>
        </ul>

      

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link text-white" data-toggle="dropdown" href="#">
              <i class="far fa-comments "></i>
              <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Brad Diesel
                      <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">Call me whenever you can...</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      John Pierce
                      <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">I got your message bro</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <img src="../../../DESIGN/assets/bootstrap-4.5.0-dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Nora Silvester
                      <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">The subject goes here</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link text-white" data-toggle="dropdown" href="#">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">15 Notifications</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> 4 new messages
                <span class="float-right text-muted text-sm">3 mins</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> 8 friend requests
                <span class="float-right text-muted text-sm">12 hours</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> 3 new reports
                <span class="float-right text-muted text-sm">2 days</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
          </li>

          

        </ul>
      </nav>
      <!-- /.navbar -->
      <?php include '../../DROPDOWN/supervisor_menu.php';
      echo (basename($_SERVER['PHP_SELF']));
        ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark"></h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                  <li class="breadcrumb-item active">Detalles de suscripcion</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->



        <div class="card card-body col-sm-12 shadow p-2 mb-5 bg-white rounded border">

        
                        <div class="col-sm-12 my-4">
                          <div class="row">    
                            <h3 class="mr-auto ml-4"><b>Detalles de suscripci&oacute;n</b></h3> 
                            <a href="my_subscriptions.php" class="btn btn-primary ml-auto mr-4">Volver a mis suscripciones</a>
                          </div>
                        </div>
                            

            <div class="row">
                <div class="col-sm-6">
                    <div class="card-body">
                        <div class="card-header bg-primary">
                            <label class="lead" for="d_cliente">Datos cliente</label>
                        </div>
                        <?php 

                        include('../../../CONNECTION/SECURITY/conex.php');

                        //tabla datos cliente
                        if(isset($_GET['id'])){
                            $id = $_GET['id'];
                            $sql = "SELECT * from datos_cliente where id_datClie = $id";
                            $query = mysqli_query($conex, $sql);
                        if(mysqli_num_rows($query)==1){
                            $row=mysqli_fetch_array($query);
                            $tipo_persona=$row['tipo_cliente'];
                            $nombre=$row['nombre'];
                            $tipo_documento=$row['tipo_documento'];
                            $num_documento=$row['num_documento'];
                            $medio_contact=$row['medio_contact'];
                            $tel_casa=$row['tel_casa'];
                            $celular=$row['celular'];
                            $fech_nacimiento=$row['fech_nacimiento'];
                            $email=$row['email'];
                            $pagador_mismo=$row['pagador_mismo'];
                        }
                        }

                        ?>
                        <ul class="list-group">
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Tipo de Persona:</label><?= $tipo_persona?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Nombre:</label><?= $nombre?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Tipo de documento:</label><?= $tipo_documento?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Numero de documento:</label><?= $num_documento?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Telefono casa:</label><?= $tel_casa?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Celular:</label><?= $celular?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Fecha de nacimiento:</label><?= $fech_nacimiento?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Email:</label><?= $email?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Pagador mismo:</label><?= $pagador_mismo?></li>
                        </ul>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="card-body">
                        <div class="card-header bg-primary ">
                            <label class="lead" for="d_cliente">Datos Suscripcion</label>
                        </div>
                        <?php 

                        include('../../../CONNECTION/SECURITY/conex.php');

                        //tabla datos suscripcion
                        if(isset($_GET['id'])){
                            $id = $_GET['id'];
                            $sql = "SELECT * from datos_suscripcion where	id_datSuscripcion = $id";
                            $query = mysqli_query($conex, $sql);
                        if(mysqli_num_rows($query)==1){
                            $row=mysqli_fetch_array($query);
                            $suscripcion=$row['suscripcion'];
                            $fecha_venta=$row['fecha_venta'];
                            $fecha_ini_suscripcion=$row['fecha_ini_suscripcion'];
                            $publicacion=$row['publicacion'];
                            $periodo_suscripcion=$row['periodo_suscripcion'];
                            $frecuencia=$row['frecuencia'];
                            $autoriza=$row['autoriza'];
                            $fecha_autoriza=$row['fecha_autoriza'];
                            $fecha_registro=$row['fecha_registro'];
                        }
                        }

                        ?>
                        <ul class="list-group">
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Suscripcion:</label><?= $suscripcion?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Fecha de venta:</label><?= $fecha_venta?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Fecha inicial de suscripcion:</label><?= $fecha_ini_suscripcion?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Publicacion:</label><?= $publicacion?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Periodo de Suscripcion:</label><?= $periodo_suscripcion?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Frecuencia:</label><?= $frecuencia?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Autoriza:</label><?= $autoriza?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Fecha de autorizacion:</label><?= $fecha_autoriza?></li>
                          <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Fecha de registro:</label><?= $fecha_registro?></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                      <div class="card-body">
                          <div class="card-header bg-primary ">
                              <label class="lead" for="d_cliente">Formas de Pago</label>
                          </div>
                          <?php 

                          include('../../../CONNECTION/SECURITY/conex.php');

                          //tabla datos forma de pago
                          if(isset($_GET['id'])){
                            $id = $_GET['id'];
                            $sql = "SELECT * FROM forma_pago WHERE id_formaPago = $id";
                            $query = mysqli_query($conex, $sql);
                          if(mysqli_num_rows($query)==1){
                              $row=mysqli_fetch_array($query);
                              $medio_pago = $row['medio_pago'];
                              $noCuotas = $row['num_cuotasTC'];
                              $valorNeto = $row['valor_neto'];
                              $descuento = $row['descuento'];
                        
                              
                              ?>

                          <ul class="list-group">
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Medio de pago:</label><?= $medio_pago?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Numero de coutas:</label><?= $noCuotas?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Valor neto:</label><?= $valorNeto?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Descuento:</label><?= $descuento?></li>
                            
                          </ul>
                        <?php
                        }
                        }

                        ?>
                        
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="card-body">
                          <div class="card-header bg-primary ">
                              <label class="lead" for="d_cliente">Direcciones de suscriptor</label>
                          </div>
                          <?php 

                          include('../../../CONNECTION/SECURITY/conex.php');

                          //tabla datos direccion suscripcion
                          if(isset($_GET['id'])){
                            $id = $_GET['id'];
                            $sql = "SELECT * FROM direccion_suscripcion WHERE id_direcSuscripcion = $id";
                            $query = mysqli_query($conex, $sql);
                          if(mysqli_num_rows($query)==1){
                              $row=mysqli_fetch_array($query);
                              $departamentosus = $row['departamento'];
                              $ciudadsus = $row['ciudadsus'];
                              $barrio = $row['barrio'];
                              $direccion = $row['DIRECCION'];
                        
                              
                              ?>

                          <ul class="list-group">
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Departamento:</label><?= $departamentosus?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Ciudad:</label><?= $ciudadsus?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Barrio:</label><?= $barrio?></li>
                            <li class="list-group-item"><label class="mr-2" for="tipo_cliente">Direccion:</label><?= $direccion?></li>
                           
                          </ul>
                        <?php
                        }
                        }

                        ?>
                        
                      </div>
                  </div>
              </div>
            </div>  

                
              </div>  
          </div>
      </div>

















         
             <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?php date_default_timezone_set('America/Bogota');
                              echo date('Y'); ?> <a target="_blank" href="https://peoplemarketing.com">People Marketing SAS</a>.</strong>
    All rights reserved.

  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->





  </body>

</html>
<?php  } else {
  echo 'usted no tiene permiso para esta informaci&oacute;n';
}  ?>