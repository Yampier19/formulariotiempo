<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Form EL TIEMPO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <meta name="mobile-web-app-capable" content="yes">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../DESIGN/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../DESIGN/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../DESIGN/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  <script src="../../DESIGN/JS/jquery-3.5.1.min.js"></script>

  
  <!-- LOGIN ANTIGUO CON ZEBRA SESSION
  
  
  <script type="text/javascript">
    function ident() {
        var dXNlcmxvZw = $('#dXNlcmxvZw').val();
		var cGFzc3dvcmRsb2c = $('#cGFzc3dvcmRsb2c').val();
	      
        $.ajax(
                {
                    url:'../../FUNCTIONS/CRUD/login_user.php',

                        data:
                                {
                                    dXNlcmxvZw: dXNlcmxvZw,
									cGFzc3dvcmRsb2c: cGFzc3dvcmRsb2c
									
                                },
                        type: 'post',
                        success: function (data)
                        {
                            $('#alerta').html(data);
                        }
                    });
        }
</script> 
<script type="text/javascript">
    $(document).ready(function () {
   $('#enviar').click(function ()
        {
            ident();
        });  



} );
</script> 

-->

<style>
   
   body {
   /* Valores de atributo clave */
overscroll-behavior: auto; 
overscroll-behavior: contain;
overscroll-behavior: none;

/* Dos valores (X y Y respectivamente) */
overscroll-behavior: auto contain;

/* Valores Globales */
overflow: inherit;
overflow: initial;
overflow: unset;
  overscroll-behavior: contain;
}</style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><img src="../../DESIGN/IMG/ELTIEMPO-Azul.png" width="80%"> </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Formulario de suscripci&oacute;n </p>

      <form action="./../../FUNCTIONS/LOGIN/login.php" method="POST">

        <div class="input-group mb-3">
          <input type="text" class="form-control" name="dXNlcmxvZw"  id="dXNlcmxvZw"  placeholder="Usuario" required = "required">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user-shield"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control" name="cGFzc3dvcmRsb2c" id="cGFzc3dvcmRsb2c" placeholder="Password..." required = "required">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">

          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Recordarme
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
		        <button name="enviar" id="enviar" class="btn btn-primary btn-block">Ingresar</button>
          </div>

        </form>
       
		  <br><br>
          <!-- /.col -->
		  <div id="alerta"></div>
     <br>
<img src="../../DESIGN/IMG/Logo People.png" width="40%" height="40%">
      <br>
      <strong>Copyright &copy; <?php date_default_timezone_set('America/Bogota'); echo date('Y'); ?> <a target="_blank" href="https://peoplemarketing.com">People Marketing SAS</a>.</strong>
    </div>
    <!-- /.login-card-body -->
	
  </div>
  
</div>
<!-- /.login-box -->

<!--
<div class="row my-3 bg-dark p-3">
  <div class="col mx-auto">
    <form action="" method="POST" >
            <label for="" class="mx-auto text-center lead">Registrar usuario</label>
            <input type="text" name="name_user" placeholder="Nombre de usuario" class="form-control mb-2">
            <input type="text" name="password" placeholder="Contraseña" class="form-control mb-2">
            <input type="text" name="id_user" value="1" disabled class="form-control mb-2">
            <input type="text" name="id_loginrol" value="1" disabled class="form-control mb-2">
            <input type="text" name="activo" value="1" disabled class="form-control mb-2">

            <button type="submit" name="registrar" class="btn btn-success btn-block mx-auto my-3">Registrar</button>
    </form>
  </div>
</div>

<?php

/*
include('./../../CONNECTION/SECURITY/conex.php');


  if(isset($_POST['registrar'])){

    $name_user = $_POST['name_user'];
    $password = $_POST['password'];
    $encrypt_pass = base64_encode(MD5($password));
    $id_user = 1;
    $id_loginrol = 1;
    $activo = 1;

    $sql = "INSERT INTO `userlogin`(`name_user`, `password`, `id_user`, `id_loginrol`, `activo`) VALUES ('$name_user','$encrypt_pass','$id_user','$id_loginrol','$activo')";
    
    if (mysqli_query($conex, $sql)) {
      echo "<script>alert('Usuario registrado')</script>";
    } else {
      echo "<script>alert('Error: ' . $sql . '<br>' . mysqli_error($conex) . ')</script>";
    }
  }

  */
?>

-->
      

<!-- jQuery -->
<script src="../../DESIGN/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../DESIGN/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../DESIGN/dist/js/adminlte.min.js"></script>

</body>
</html>
