// JavaScript Document
$(document).ready(function () {
  $("#direccionAlternaSi").on("click", function () {
    $('#target').show(); //muestro mediante id
    $('.target').show(); //muestro mediante clase
  });
  $("#direccionAlternaNo").on("click", function () {
    $('#target').hide(); //oculto mediante id
    $('.target').hide(); //muestro mediante clase
  });

  $("#otroSuscrip").on("click", function () {
    $('#cualSuscrip').show(); //muestro mediante id
    $('.cualSuscrip').show(); //muestro mediante clase
  });
  $("#unMes").on("click", function () {
    $('#ocultarSuscrip').hide(); //oculto mediante id
    $('.ocultarSuscrip').hide(); //muestro mediante clase
  });
  $("#tresMeses").on("click", function () {
    $('#ocultarSuscrip').hide(); //oculto mediante id
    $('.ocultarSuscrip').hide(); //muestro mediante clase
  });
  $("#seisMeses").on("click", function () {
    $('#ocultarSuscrip').hide(); //oculto mediante id
    $('.ocultarSuscrip').hide(); //muestro mediante clase
  });
  $("#doceMeses").on("click", function () {
    $('#ocultarSuscrip').hide(); //oculto mediante id
    $('.ocultarSuscrip').hide(); //muestro mediante clase
  });

  $("#mismoSuscripNo").on("click", function () {
    $('#targetSuscrip').show(); //muestro mediante id
    $('.targetSuscrip').show(); //muestro mediante clase
  });
  $("#mismoSuscripSi").on("click", function () {
    $('#targetSuscrip').hide(); //oculto mediante id
    $('.targetSuscrip').hide(); //muestro mediante clase
  });

  $("#otraFrecuencia").on("click", function () {
    $('#cualFrecuencia').show(); //muestro mediante id
    $('.cualFrecuencia').show(); //muestro mediante clase
  });
  $("#todoslosdias").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#lunes").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#martes").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#miercoles").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#jueves").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#viernes").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#sabado").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#domingo").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#lunesasabado").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#quincenal").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#mensual").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });
  $("#bimensual").on("click", function () {
    $('#cualFrecuencia').hide(); //oculto mediante id
    $('.cualFrecuencia').hide(); //muestro mediante clase
  });

  $("#otrotargeta").on("click", function () {
    $('#cualTargeta').show(); //muestro mediante id
    $('.cualTargeta').show(); //muestro mediante clase
  });
  $("#visa").on("click", function () {
    $('#cualTargeta').hide(); //oculto mediante id
    $('.cualTargeta').hide(); //muestro mediante clase
  });
  $("#mastercard").on("click", function () {
    $('#cualTargeta').hide(); //oculto mediante id
    $('.cualTargeta').hide(); //muestro mediante clase
  });
  $("#dinners").on("click", function () {
    $('#cualTargeta').hide(); //oculto mediante id
    $('.cualTargeta').hide(); //muestro mediante clase
  });
  $("#credencial").on("click", function () {
    $('#cualTargeta').hide(); //oculto mediante id
    $('.cualTargeta').hide(); //muestro mediante clase
  });
  $("#ae").on("click", function () {
    $('#cualTargeta').hide(); //oculto mediante id
    $('.cualTargeta').hide(); //muestro mediante clase
  });

  $("#targetaCredito").on("click", function () {
    $('#ocultar').show(); //muestro mediante id
    $('.ocultar').show(); //muestro mediante clase
  });
  $("#efectivo").on("click", function () {
    $('#ocultar').hide(); //oculto mediante id
    $('.ocultar').hide(); //muestro mediante clase
  });
  $("#cheque").on("click", function () {
    $('#ocultar').hide(); //oculto mediante id
    $('.ocultar').hide(); //muestro mediante clase
  });
  $("#otros-pagos").on("click", function () {
    $('#ocultar').hide(); //oculto mediante id
    $('.ocultar').hide(); //muestro mediante clase
  });
  $("#medioPagoSi").on("click", function () {
    $('#ocultarOtro').show(); //muestro mediante id
    $('.ocultarOtro').show(); //muestro mediante clase
  });
  $("#efectivo").on("click", function () {
    $('#ocultarOtro').hide(); //oculto mediante id
    $('.ocultarOtro').hide(); //muestro mediante clase
  });
  $("#cheque").on("click", function () {
    $('#ocultarOtro').hide(); //oculto mediante id
    $('.ocultarOtro').hide(); //muestro mediante clase
  });
  $("#targetaCredito").on("click", function () {
    $('#ocultarOtro').hide(); //oculto mediante id
    $('.ocultarOtro').hide(); //muestro mediante clase
  });

  $("#obsequioSi").on("click", function () {
    $('#obsequioCual').show(); //muestro mediante id
    $('.obsequioCual').show(); //muestro mediante clase
  });
  $("#obsequioNo").on("click", function () {
    $('#obsequioCual').hide(); //oculto mediante id
    $('.obsequioCual').hide(); //muestro mediante clase
  });

  $("#otroPublicacion").on("click", function () {
    $('#cualOtro').show(); //muestro mediante id
    $('.cualOtro').show(); //muestro mediante clase
  });
  $("#eltiempo").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#eltiempoGgPlatino").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#eltiempoDgOro").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#portafolio").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#revistaPortafolio").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#revistaAlo").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#revistaDonJuan").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#revistaHabitar").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });
  $("#revistaBocas").on("click", function () {
    $('#cualOtro').hide(); //oculto mediante id
    $('.cualOtro').hide(); //muestro mediante clase
  });


});