
$(document).ready(function() {
  var via = $('#VIA').val();
  var dt_via = $('#detalle_via').val();
  $('#VIA').change(function() {
    dir();
  });

  $('#detalle_via').change(function() {
    dir();
  });
  $('#numero').change(function() {
    dir();
  });
  $('#numero2').change(function() {
    dir();
  });
  $('#interior').change(function() {
    dir();
  });
  $('#detalle_int').change(function() {
    dir();
  });
  $('#interior2').change(function() {
    dir();
  });
  $('#detalle_int2').change(function() {
    dir();
  });
  $('#interior3').change(function() {
    dir();
  });
  $('#detalle_int3').change(function() {
    dir();
  });

  $('#ciudad').select2();

  /////////////////////Terminacion///////////////// 



});

$(document).ready(function() {

  var viaAL = $('#VIAAL').val();
  var dt_viaAL = $('#detalle_viaAL').val();
  $('#VIAAL').change(function() {
    dirAL();
  });

  $('#detalle_viaAL').change(function() {
    dirAL();
  });
  $('#numeroAL').change(function() {
    dirAL();
  });
  $('#numero2AL').change(function() {
    dirAL();
  });
  $('#interiorAL').change(function() {
    dirAL();
  });
  $('#detalle_intAL').change(function() {
    dirAL();
  });
  $('#interior2AL').change(function() {
    dirAL();
  });
  $('#detalle_int2AL').change(function() {
    dirAL();
  });
  $('#interior3AL').change(function() {
    dirAL();
  });
  $('#detalle_int3AL').change(function() {
    dirAL();
  });

  $('#ciudadAL').select2();
});


$(document).ready(function() {
  $('#publicacion').change(function() {

    var publicacion = $('#publicacion').val();
    if (publicacion == 'OTRO') {

      $("#cualOtroPu").css('display', 'block');
      $('#cualOtroPu').attr('required', 'required');
    } else {
      $("#cualOtroPu").css('display', 'none');
      $("#cualOtroPu").val("");
      $('#cualOtroPu').removeAttr('required', 'required');

    }
  });

  $('#periodoSuscrip').change(function() {

    var periodoSuscrip = $('#periodoSuscrip').val();
    if (periodoSuscrip == 'OTRO') {

      $("#periodoOt").css('display', 'block');
      $('#periodoOt').attr('required', 'required');
    } else {
      $("#periodoOt").css('display', 'none');
      $("#periodoOt").val("");
      $('#periodoOt').removeAttr('required', 'required');

    }
  });

  $('#tipoTargeta').change(function() {

    var tipoTargeta = $('#tipoTargeta').val();
    if (tipoTargeta == 'otrotargetas') {

      $("#cualTargeta").css('display', 'block');
      $('#cualTargeta').attr('required', 'required');
    } else {
      $("#cualTargeta").css('display', 'none');
      $("#cualTargeta").val("");
      $('#cualTargeta').removeAttr('required', 'required');

    }
  })

  $('#targetaCredito').change(function() {

    var targetaCredito = $('#targetaCredito').val();
    if (targetaCredito == 'TARJETA DE CREDITO') {

      $('#tipoTargeta').attr('required', 'required');
      $('#cualTargeta').attr('required', 'required');
      $('#noTargeta').attr('required', 'required');
      $('#banco').attr('required', 'required');
      $('#noCuotas').attr('required', 'required');
      $('#month').attr('required', 'required');
    } else {
      $("#cualTargeta").val("");
      $('#cualTargeta').removeAttr('required', 'required');

    }
  })

  $('#codensaSi').change(function() {

    var codensaSi = $('#codensaSi').val();
    if (codensaSi == 'codensaSi') {

      $('#nie1').attr('required', 'required');
      $('#nie2').attr('required', 'required');
    } else {
      $("#codensaSi").val("");
      $('#codensaSi').removeAttr('required', 'required');

    }
  })

  $('#medioPagoSi').change(function() {

    var medioPagoSi = $('#medioPagoSi').val();
    if (medioPagoSi == 'SI') {

      $('#medioPagoOcultarOtro').attr('required', 'required');
    } else {
      $("#medioPagoOcultarOtro").val("");
      $('#medioPagoOcultarOtro').removeAttr('required', 'required');

    }
  })


  $('#frecuencia').change(function() {

    var frecuencia = $('#frecuencia').val();
    if (frecuencia == 'OTRO') {

      $("#frecuenciaOt").css('display', 'block');
      $('#frecuenciaOt').attr('required', 'required');
    } else {
      $("#frecuenciaOt").css('display', 'none');
      $("#frecuenciaOt ").val("");
      $('#frecuenciaOt').removeAttr('required', 'required');
    }
  });

  $('#tipoDocumento').change(function() {

    var tipoDocumento = $('#tipoDocumento').val();
    if (tipoDocumento == 'OTRO') {

      $("#documenOr").css('display', 'block');
      $('#documenOr').attr('required', 'required');
    } else {
      $("#documenOr").css('display', 'none');
      $('#documenOr').removeAttr('required', 'required');
    }
  });
});

<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/sweetalert.min.js"></script>

$(document).ready(function() {


  $("#enviar").click(function() {

    //Variables
    // Seccion de la A a la D
    var suscripcion1 = $("#suscripcion").val();
    var fechaVenta1 = $("#fechaVenta").val();
    var publicacion1 = $("#publicacion").val();
    var fechaInicioSuscrip1 = $("#fechaInicioSuscrip").val();
    var periodoSuscrip1 = $("#periodoSuscrip").val();
    var frecuencia1 = $("#frecuencia").val();

    // Seccion E
    var fechaHabeas1 = $("#fechaHabeas").val()
    var tipoPersona1 = $("#tipoPersona").val();
    var tipoDocumento1 = $("#tipoDocumento").val();
    var numeroDocumentoCliente1 = $("#numeroDocumentoCliente").val();
    var medioContacto1 = $("#medioContacto").val();
    var nombreCompletoCliente1 = $("#nombreCompletoCliente").val();
    var celular1 = $("#celular").val();
    var fechaNacimiento1 = $("#fechaNacimiento").val();
    var email1 = $("#email").val();
    var estadoCivil1 = $("#estadoCivil").val();

    //INFORMACION ALTERNA
    var nit2 = $("#nit").val()
    var medioContactoAlterno2 = $("#medioContactoAlterno").val()
    var tipoDocumentoSuscrip2 = $("#tipoDocumentoSuscrip").val()
    var noDocumentoAlterno2 = $("#noDocumentoAlterno").val()
    var nombresCompletosSuscrip2 = $("#nombresCompletosSuscrip").val()
    var apellidosSuscrip2 = $("#apellidosSuscrip").val()
    var celularSuscripAlterno2 = $("#celularSuscripAlterno").val()
    var fechaNacimientoSuscrip2 = $("#fechaNacimientoSuscrip").val()
    var emailsuscripalterno2 = $("#emailsuscripalterno").val()
    var estadoCivilSuscrip2 = $("#estadoCivilSuscrip").val()


    // Funciones        
    //Seccion de la A a la D
    if (suscripcion1 == '') {
      swal({
        icon: 'warning',
        title: 'A. SUSCRIPCION NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 1500,
        timerProgressBar: true
      });

    } else if (fechaVenta1 == '') {
      swal({
        icon: 'warning',
        title: 'LA FECHA DE VENTA NO PUEDE ESTAR VACIA!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });

    } else if (publicacion1 == '') {
      swal({
        icon: 'warning',
        title: 'B. EL CAMPO DE PUBLICACIÓN NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });

    } else if (fechaInicioSuscrip1 == '') {
      swal({
        icon: 'warning',
        title: 'LA FECHA DE INICIO DE VENTA NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });

    } else if (periodoSuscrip1 == '') {
      swal({
        icon: 'warning',
        title: 'C. PERIODO DE SUSCRIPCIÓN NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });

    } else if (frecuencia1 == '') {
      swal({
        icon: 'warning',
        title: 'D. FRECUENCIA NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }
    // Seccion E
    else if (fechaHabeas1 == '') {
      swal({
        icon: 'warning',
        title: 'FECHA DE MARCACIÓN NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (tipoPersona1 == '') {
      swal({
        icon: 'warning',
        title: 'TIPO DE PERSONA NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (tipoDocumento1 == '') {
      swal({
        icon: 'warning',
        title: 'TIPO DE DOCUMENTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (numeroDocumentoCliente1 == '') {
      swal({
        icon: 'warning',
        title: 'NO. DE DOCUMENTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (medioContacto1 == '') {
      swal({
        icon: 'warning',
        title: 'MEDIO DE CONTACTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (nombreCompletoCliente1 == '') {
      swal({
        icon: 'warning',
        title: 'NOMBRE DE CLIENTE O RAZÓN SOCIAL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (celular1 == '') {
      swal({
        icon: 'warning',
        title: 'CELULAR NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (fechaNacimiento1 == '') {
      swal({
        icon: 'warning',
        title: 'FECHA DE NACIMIENTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (email1 == '') {
      swal({
        icon: 'warning',
        title: 'EMAIL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (estadoCivil1 == '') {
      swal({
        icon: 'warning',
        title: 'ESTADO CIVIL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }
    //INFORMACION ALTERNA
    /*
    else if (nit2 == ''){
      swal({
        icon: 'warning',
        title: 'NIT NO PUEDE ESTAR VACIO',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(medioContactoAlterno2 == ''){
      swal({
        icon: 'warning',
        title: 'MEDIO DE CONTACTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(tipoDocumentoSuscrip2 == ''){
      swal({
        icon: 'warning',
        title: 'TIPO DE DOCUMENTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(noDocumentoAlterno2  == ''){
      swal({
        icon: 'warning',
        title: 'NO. DE DOCUMENTO NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(nombresCompletosSuscrip2  == ''){
      swal({
        icon: 'warning',
        title: 'NOMBRE DE CLIENTE O RAZÓN SOCIAL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(apellidosSuscrip2  == ''){
      swal({
        icon: 'warning',
        title: 'APELLIDOS NO PUEDE ESTAR VACIO',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(celularSuscripAlterno2  == ''){
      swal({
        icon: 'warning',
        title: 'CELULAR NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(fechaNacimientoSuscrip2  == ''){
      swal({
        icon: 'warning',
        title: 'FECHA DE NACIMIENTO NO PUEDE ESTAR VACIO',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }else if(emailsuscripalterno2  == ''){
      swal({
        icon: 'warning',
        title: 'EMAIL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    } else if (estadoCivil1 == '') {
      swal({
        icon: 'warning',
        title: 'ESTADO CIVIL NO PUEDE ESTAR VACIO!',
        text: '',
        timer: 2000,
        timerProgressBar: true
      });
    }
*/
  });
});