
<?php
require('fpdf.php');

$pdf = new FPDF();
date_default_timezone_set('America/Bogota');
$date_hoy = date('Y-m-d');

$dia = substr($date_hoy, 8, 1);
$dia2 = substr($date_hoy, 9, 1);
$mes = substr($date_hoy, 5, 1);
$mes2 = substr($date_hoy, 6, 1);

$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf -> Image('imagen.jpg',0,-10,210,315);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(70);
//// header people
$pdf->Cell('5',-10,'People Marketing S.A.S NIT 830.028.713-6');
$pdf->Cell('5',-5,'Calle 97 # 19a - 57 Tel: 2360042');
$pdf->Ln(2);
//////// primera ilera Susucripci�n
$pdf->Cell(35.6);
$pdf->Cell('5',20.5,'X');// Nueva
$pdf->Cell(16.8);
$pdf->Cell('5',20.5,'X');// Renovacion
$pdf->Cell(87);
$pdf->SetFont('Arial','',12);
$pdf->Cell('3.5',22,$dia.' '.$dia2);
$pdf->Cell(5);
$pdf->Cell('1',22,$mes.' '.$mes2);
$pdf->Cell(16);
$pdf->Cell('1',22,'2 0');
$pdf->Ln(10.5);/// segunda ilera campo publicaci�n
$pdf->SetFont('Arial','B',7);
///radio tiempo
$pdf->Cell(9.5);
$pdf->Cell('5',20,'X');
///radio tiempo dg platino
$pdf->Cell(9);
$pdf->Cell('5',20,'X');
///radio tiempo dg oro
$pdf->Cell(11.3);
$pdf->Cell('5',20,'X');
///radio portafolio
$pdf->Cell(6);
$pdf->Cell('5',20,'X');
///radio revista portafolio
$pdf->Cell(3);
$pdf->Cell('5',20,'X');
///radio revista al�
$pdf->Cell(3.5);
$pdf->Cell('5',20,'X');
///radio don Juan
$pdf->Cell(3);
$pdf->Cell('5',20,'X');
///radio reviata Habitar
$pdf->Cell(3.7);
$pdf->Cell('5',20,'X');
///radio revista bocas
$pdf->Cell(3.5);
$pdf->Cell('5',20,'X');
///radio otro
$pdf->Cell(4);
$pdf->Cell('5',20,'X');
/// texto cual de otro
$pdf->Cell(3);
$pdf->Cell('5',20,'Ejemplo de Otro');

// Fecha inicio suscripcion
$pdf->Cell(35);
$pdf->SetFont('Arial','',12);
$pdf->Cell('3.5',18,$dia.' '.$dia2);
$pdf->Cell(5);
$pdf->Cell('1',18,$mes.' '.$mes2);
$pdf->Cell(16);
$pdf->Cell('1',18,'2 0');

///Periodo de suscripcion 
$pdf->Ln(8.2);/// tercera  ilera campo publicaci�n
$pdf->SetFont('Arial','B',7);
///radio 1 mes
$pdf->Cell(13.4);
$pdf->Cell('5',20,'X');
///radio 3 meses
$pdf->Cell(5.7);
$pdf->Cell('5',20,'X');
///radio 6 meses
$pdf->Cell(5.7);
$pdf->Cell('5',20,'X');
///radio 12 meses
$pdf->Cell(5.7);
$pdf->Cell('5',20,'X');
///radio otro
$pdf->Cell(4.2);
$pdf->Cell('5',20,'X');
/// texto cual de otro
$pdf->Cell(4);
$pdf->Cell('5',20,'Ejemplo de Otro');

//4 Ilera Frecuencia
$pdf->Ln(12.8);
$pdf->Cell(8.5);
$pdf->Cell('5',20,'X');
///radio lunes
$pdf->Cell(2.2);
$pdf->Cell('5',20,'X');
///radio Martes
$pdf->Cell(2);
$pdf->Cell('5',20,'X');
///radio Miercoles
$pdf->Cell(3.3);
$pdf->Cell('5',20,'X');
///radio Jueves
$pdf->Cell(3.3);
$pdf->Cell('5',20,'X');
///radio Viernas
$pdf->Cell(2.9);
$pdf->Cell('5',20,'X');
///radio Sabado
$pdf->Cell(2.9);
$pdf->Cell('5',20,'X');
///radio Domingo
$pdf->Cell(4);
$pdf->Cell('5',20,'X');
///radio Lunes a sabado
$pdf->Cell(4);
$pdf->Cell('5',20,'X');
///radio Quincena
$pdf->Cell(5.7);
$pdf->Cell('5',20,'X');
///radio Mensual
$pdf->Cell(6.7);
$pdf->Cell('5',20,'X');
///radio Bimensual
$pdf->Cell(6.6);
$pdf->Cell('5',20,'X');
///radio Otro
$pdf->Cell(4.3);
$pdf->Cell('5',20,'X');
/// texto cual de otro
$pdf->Cell(7);
$pdf->Cell('5',20,'Ejemplo de Otro');

///radio ser contactado E-mail
$pdf->Cell(10.6);
$pdf->Cell('5',41.5,'X');
///radio ser contactado celular
$pdf->Cell(5.7);
$pdf->Cell('5',41.5,'X');
///radio ser contactado tel casal
$pdf->Cell(6.3);
$pdf->Cell('5',41.5,'X');
///radio ser contactado tel oficina
$pdf->Cell(8);
$pdf->Cell('5',41.5,'X');


///Informacion del cliente
$pdf->Ln(10.8);
///radio Persona Natural
$pdf->Cell(9);
$pdf->Cell('5',20,'X');
///radio Persona Juridico
$pdf->Cell(3.8);
$pdf->Cell('5',20,'X');
///radio CC
$pdf->Cell(8.2);
$pdf->Cell('5',20,'X');
///radio CE
$pdf->Cell(2.8);
$pdf->Cell('5',20,'X');
///radio TI
$pdf->Cell(2.3);
$pdf->Cell('5',20,'X');
///radio NIT
$pdf->Cell(2.8);
$pdf->Cell('5',20,'X');

/// Numero de documento
$pdf->Cell(3);
$str = '10318438869589565';
$caracteres = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
$longitud = strlen ($str);
for($i = 0; $i < $longitud; $i++){
$pdf->Cell(1.9);
$pdf->Cell('1.9',20,$caracteres[$i]);
}

/// nombre completo /razon social
$pdf->Ln(8.3);
$pdf->Cell(6);
$nombre = 'JAIVER YAMPIER HERRERA LOZANO';
$caracteresn = preg_split('//', $nombre, -1, PREG_SPLIT_NO_EMPTY);
$longitudn = strlen ($nombre);
for($i = 0; $i < $longitudn; $i++){ 
$pdf->Cell(1.9);
$pdf->Cell('1.9',20,$caracteresn[$i]);
}

/// Telefono casa
$pdf->Ln(7.5);
$pdf->Cell(5.5);
$tel = '10318438869589565';
$caracterest = preg_split('//', $tel, -1, PREG_SPLIT_NO_EMPTY);
$longitudt = strlen ($tel);
for($i = 0; $i < 10; $i++){
$pdf->Cell(1.9);
$pdf->Cell('1.9',20,$caracterest[$i]);
}

/// Telefono oficina
$pdf->Cell(3);
$telO = '10318438869589565';
$caracterestO = preg_split('//', $tel, -1, PREG_SPLIT_NO_EMPTY);
$longitudtO = strlen ($telO);
for($i = 0; $i < 10; $i++){
$pdf->Cell(1.9);
$pdf->Cell('1.9',20,$caracterestO[$i]);
}
$pdf->Cell(6);
$pdf->Cell('5',20,'2   5    8   6');

/// Telefono celular
$pdf->Cell(11);
$cel = '10318438869589565';
$caracterestce = preg_split('//', $cel, -1, PREG_SPLIT_NO_EMPTY);
$longitudtce = strlen ($cel);
for($i = 0; $i < 10; $i++){
$pdf->Cell(1.9);
$pdf->Cell('1.9',20,$caracterestce[$i]);
}

/// Fecha de Nacimiento
$pdf->Cell(4);
$pdf->SetFont('Arial','',12);
$pdf->Cell('3.5',20,$dia.' '.$dia2);
$pdf->Cell(5);
$pdf->Cell('1',20,$mes.' '.$mes2);
$pdf->Cell(9);
$pdf->Cell('1',20,'2 0 2 0');


$pdf->Output();

?>