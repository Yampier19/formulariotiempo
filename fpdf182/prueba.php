
<?php
require('fpdf.php');

$pdf = new FPDF();
date_default_timezone_set('America/Bogota');
$date_hoy = date('Y-m-d');

$dia = substr($date_hoy, 8, 1);
$dia2 = substr($date_hoy, 9, 1);
$mes = substr($date_hoy, 5, 1);
$mes2 = substr($date_hoy, 6, 1);

$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf -> Image('imagen.jpg',0,-10,210,315);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(70);
$pdf->Cell('5',-10,'People Marketing S.A.S NIT 830.028.713-6');

$pdf->Cell('5',-5,'Calle 97 # 19a - 57 Tel: 2360042');
$pdf->Ln(2);
$pdf->Cell(35.5);
$pdf->Cell('5',20,'X');
$pdf->Cell(16.8);
$pdf->Cell('5',20,'X');
$pdf->Cell(87);
$pdf->SetFont('Arial','',12);
$pdf->Cell('3.5',22,$dia.' '.$dia2);
$pdf->Cell(5);
$pdf->Cell('1',22,$mes.' '.$mes2);
$pdf->Cell(16);
$pdf->Cell('1',22,'2 0');
$pdf->Output();
?>